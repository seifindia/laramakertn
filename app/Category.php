<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

     /* The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';

protected $fillable = ['title'];
    
    public function blocks() {
    	return $this->hasMany(Block::class, 'id_categorie');
    }
    
    public function pages() {
            return $this->belongsToMany(Page::class, 'category_page');
        }
}
