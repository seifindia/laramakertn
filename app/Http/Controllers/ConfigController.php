<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\EnquiryNow;
use App\Company;
use App\MenuType;
use App\Page;
use App\Menu;
class ConfigController extends MainBaseController
{

    /**
    * Load languages from MainBaseController
    *
    * @return void
    */
    public function __construct() {
        parent::__construct();
    }

    /**
    * Load languages, menytypes, pages and menus
    *
    *
    * @return array
    */
    public function index() {
        $menutypes = MenuType::all();
        $menus = Menu::all();
        $pages = Page::all();   
        return view('configs', 
                ['languages'=>$this->languages,
                 'menutypes'=> $menutypes,
                 'pages'=>$pages,
                 'menus'=>$menus]);
    }
    
    /**
    * Write to file 
    * save css and js files
    * @param file_name
    * @return redirection
    */
    public function saveFile(Request $request) {
    	$file = $request->file_name;
     	$bytes_written = \File::put($file, $request->content);
		if ($bytes_written === false)
		{
		  die("Error writing to file");
		}
        return redirect()->action('ConfigController@index', []);
    }

    /**
    * Save company information
    * Only one row is considered
    * save css and js files
    * @param request
    * @return redirection
    */
    public function storeCompany(Request $request) {
        $company = new Company;
        $company->full_name = $request->full_name;
        $company->short_name = $request->short_name;
        $company->email = $request->email;
        $company->url = $request->url;
        if ($request->hasFile('favicon')) 
        {
            $path = $request->file('favicon')->storeAs('','favicon.ico');
            $company->favicon = $path;
        }
        $company->adress = $request->adress;
        $company->phone = $request->phone;
        $company->description = $request->description;
        $company->youtube = $request->youtube;
        $company->facebook = $request->facebook;
        $company->twitter = $request->twitter;
        $company->linkedin = $request->linkedin;
        $company->save();
        return redirect()->action('ConfigController@index', []);
    }    

    /**
    * update company information
    * 
    * always update first row
    * used update all
    * @param request
    * @return redirection
    */
    public function UpdateCompany(Request $request) {
        $company = Company::first();
         if ($request->hasFile('favicon')) 
        {
            $path = $request->file('favicon')->storeAs('','favicon.ico');
            $company->favicon = $path;
        }
        $company->update($request->all());

        return redirect()->action('ConfigController@index', []);
    }

    /**
    * Send Email when any form is filled
    * the send method shoudld receive a Mailable instance (app/Mail/...)
    * the to method receive the reception email (can be configured/created to be in config/mail/)
    * the from email and name can be found in  config/mail/
    * @param request
    * @return redirection
    */
    public function sendEmail(Request $reuest) {
        Mail::to("seifallah.ba@gmail.com")->send(new EnquiryNow($request));
        return redirect()->action('ConfigController@index', []);
    }

    /**
    * Click and backup your database
    * filename will be generated autmatically
    * this method redirect to backup.php whic is written in pure php,
    * it should be update to support laravel
    * new tab will be opened and closed autmatically using javascript
    * @param void
    * @return redirection to php file
    */

    public function backupdb() {
        return redirect('../backup.php');
    }
    
}
