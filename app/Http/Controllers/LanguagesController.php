<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Language;
use App\Equivalance;
use App\Block;
use App\Page;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Migrations\Migration;

class LanguagesController extends MainBaseController
{
    private $colang;

    public function __construct() {
        parent::__construct();
    }

    public function index() {
    	return view('languages', ['languages'=> $this->languages]);
    }
    /**
    * Store new language maisn to 
    *
    * Duplicate existing pages with new code_lang via these steps:
    * 1.add new language
    * 2.alter the structure of equivalances tbl by adding new coloumn with value of code_lang and row value is zero
    * 3.List all columns of Equivalances tbl
    * 4. Create new instance of Equivalance
    * 5. Get all languages
    * 6.Foreach language and add missing pages
    * 7.add blocks
    * @param request
    * @return redirection
    */
    public function store(Request $request) {
        /**
        *1.add new language
        */

        $colang = 'hlif';
    	$language = new Language;
    	$language->name = $request->name;
    	$language->code_lang = $request->code_lang; 
        if ($request->hasFile('flag_img')) {
            $path = $request->file('flag_img')->store('images/flags');
            $language->flag_img = $path;
        }
        $rst = $request->file('flag_img');
        $language->save();
        
        /**
        *2.alter the structure of equivalances tbl
        */
        $colang = $request->code_lang; 
        Schema::table('equivalances', function (Blueprint $table) use ($colang) {
            $table->smallInteger($colang);
        });

        /**
        *3. Preparation 
        */
        $list = Schema::getColumnListing('equivalances');    
        $equiv0 = new Equivalance;
        $languages =Language::all();
        $newBlockTask ='';

        /**
        *4.boucle to add pages
        */

        foreach ($languages as $lang) {
            //get all rows that contains zero in their languages columns
            $zeroPages = Equivalance::where($lang->code_lang,'0')->get(); 
            foreach ($zeroPages as $key => $value) {
                //get only the columns that have zero as value
                $idlang = $lang->code_lang;
                // echo "id: "+$value->id."--------col:".$idlang." -------val: ".$value->en."<br>";
                //get value of first language column and duplicate page and update equivalance
                //1.get the id of the first column language then duplicate that has zero

                $tasks = Page::find($value->$list[3]);
                $newTask = $tasks->replicate();
                $newTask->title = $tasks->title."_".$idlang;
                $newTask->code_lang = $idlang;
                $newTask->page_url = $idlang."/".$newTask->title;
                $newTask->save();
                $idpage   = $newTask->id;  

                //2.get last id for the new page and update equivalance tbl         
                $equiv0->where('id', $value->id)->update([$idlang => $idpage]);
                //echo "<br>"."Update *** id ".$value->id." *** lang:***".$idlang."**".$idpage."**********<br>";  
            }
      
        }

        /**
        *5.Add blocks with new language
        * 
        * foreach block find equivalnce page_id then update it
        * check if there are blocks then
        * get blocks in first lang
        * loop through returned blocks
        * get equivalnce pages_id of new language
        * update fields
        * $newBlockTask->page_id will be the vakue of last column in equivalance
        */


        if (Block::count() > 0) {

            $blocks_current_task = Block::where([['code_lang', $list[3]]])->get();
            foreach ($blocks_current_task as $block_task) {
                $equiv1 =   Equivalance::where($list[3],$block_task->page_id)->first();
               // dd($equiv1->$list[sizeof($list)-1]);
               // dd($equiv1[$list[sizeof($list)-1]]);
                $newpageid = $equiv1[$list[sizeof($list)-1]];
                $newBlockTask = $block_task->replicate();
                $newBlockTask->sub_title        = $block_task->sub_title."_".$idlang;
                $newBlockTask->code_lang    = $idlang;
                $newBlockTask->page_id      =  $newpageid; //last lang column value
                $newBlockTask->save();
                $newpageid  ="";
            }  
        }

        return redirect()->action('LanguagesController@index',[]);
    }

    /**
    * Delete Language and releated data
    *
    * Deleting language will also delete all pages, blocks in that language
    * Also, the schema of equivalance tbl will change on deleting that lang column
    */
    public function remove(Language $lang) {
        $colang2 = 'hlif';
        $b = Language::find($lang->id);
        $colang2 = $b->code_lang;
        Schema::table('equivalances', function (Blueprint $table) use ($colang2) {
            $table->dropColumn($colang2);
        });
        $b->delete();
        $pages2delete = Page::where('code_lang', $colang2)->get();
                foreach ($pages2delete as $page2delete) {
                    $page2delete->delete();
                }
        $blocks2delete = Block::where('code_lang', $colang2)->get();
                foreach ($blocks2delete as $block2delete) {
                    $block2delete->delete();
                }
        return redirect()->action('LanguagesController@index',[]);
    }
}
