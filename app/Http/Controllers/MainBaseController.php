<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Block;
use App\Language;
use App\Equivalance;
use App\Company;
class MainBaseController extends Controller
{

    /* vars should be always protected */
    protected $languages;


    public function __construct() {

        if (strpos (url()->full(), 'vw')!== false) {
            config(['database.default' => 'mysql_vw']);
            config(['app.name' => 'VadodaraWindows']);
        }
        elseif(strpos (url()->full(), 'wmm')!== false) {
            config(['database.default' => 'mysql_wmm']);
            config(['app.name' => 'Windowmaker Measure']);
        }
        else {
            config(['database.default' => 'mysql_wm']);
            config(['app.name' => 'Windowmaker']);
        }

        $this->middleware('auth');
        $this->languages = Language::all();


    }
}
