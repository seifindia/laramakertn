<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\User;

class TestController extends Controller
{
    //

    public function index() {
    	$nom = '<u>Seif Allah</u>';
    	$people = [];    	
    	$people = ['A', 'B', 'C', 'W'];
    //	return view('test')->with('mynom',$nom);
    	return view('test', compact('people', 'nom'));
    }

    public function showUsers(){
    	$users = User::all();
    	return view('test', compact('users'));
    }

    public function showUser($id){

    	$user = User::findOrFail($id);
    	
    	//dump & die
    	//dd($user);

    	return view('profile', compact('user'));
    }

    /*
// Controller
Route::get('/custom_paint', array('as' => 'custom_paint', 'uses' => 'PagesController@services'));

// View
<a href="{{ URL::route('custom_paint') }}#id">LINK</a>


*/
}
