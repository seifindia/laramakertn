<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MediaController extends MainBaseController
{
    
    public function __construct() {
        parent::__construct();
    }
        /**
        * List all files by type
        *
        *provide the path to folder staring from public path
        *
        *@return Files
        */
        public function index() {
			$pdffiles 	= \File::allFiles("front/pdf");
			$imgfiles 	= \File::allFiles("front/images");
			$cssfiles 	= \File::allFiles("front/css");
			$jsfiles 	= \File::allFiles("front/js");
			$assetfiles = \File::allFiles("front/assets");
			
        	return view('media',
			[
			'pdfiles'=>$pdffiles,
			'imagesfiles'=>$imgfiles,
			'cssfiles'=>$cssfiles,
			'jsfiles'=>$jsfiles,
			'assetfiles'=>$assetfiles
			]);
        }


        /**
        * List all files by type
        *
        *provide the path to folder staring from public path
        *@param path folder
        *@return Images
        */
		public function listImages() {
            $files = collect(\File::allFiles("front/images"))
                  ->sortByDesc(function ($file) {
                        return $file->getCTime();
                    });
        	return view('mediaselect',['imagesfiles'=>$files]);
        }

        /**
        * Upload files using Ckeditor plugin
        *
        */
        public function upload(Request $request) {
    		if ($request->hasFile('mediafile')) {
    			$folder = $request->folder;
                $path = $request->file('mediafile')->store($folder);
            }
            if (isset($request->file_toshow)) {
            	return \Redirect::to('manager/mediaselect?CKEditor=editor1&CKEditorFuncNum=1&langCode=en');
            }
            else {
                return redirect()->action('MediaController@index',[]);
            }
        }

        /**
        * Remove single file
        *
        *@param filepath
        *@return redirect
        */
        public function remove(Request $request) {
            \File::delete($request->filepath);
			    return redirect()->action('MediaController@index',[]);
        }    
}
