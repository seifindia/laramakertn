<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        if (strpos (url()->full(), 'vw')!== false) {
         //   $this->url =  "TRUUUEEE";
            config(['database.default' => 'mysql_vw']);
            config(['app.name' => 'VadodaraWindows']);
        }
        else if(strpos (url()->full(), 'wmm')!== false) {
         //   $this->url =  "FAAALLLSSEE";
            config(['database.default' => 'mysql_wmm']);
            config(['app.name' => 'Windowmaker Measure']);
            config(['filesystems.disks.local.root' => storage_path('../../htdocs/laramaker_wm/front/')]);
            // /home/vwindows2017/www/wmm/
        }
        else {
            config(['database.default' => 'mysql_wm']);
            config(['app.name' => 'Windowmaker']);
        }

        $this->middleware('guest', ['except' => 'logout']);
    }
}
