<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
       //
    public function index() {
    	$categories = Category::all();
    	return view('categories')->with('categories',$categories);;
    }

    public function store(Request $request) {
    	$category = new Category;
        $category->title = str_replace(" ","-",($request->category_name));
        $category->save();
       return redirect()->action('BlocksController@index', []);
    }
    
    public function show(Category $category) {
    	return view('relations', compact('category'));
    }

}
