<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Page;
use App\Block;
use App\Language;
use App\Equivalance;
use Illuminate\Support\Facades\Auth;
use Storage;

class PagesController extends MainBaseController
{

    public function __construct() {
        parent::__construct();
    }

    /**
    *Load All pages
    *
    *@return Pages
    */
    public function index() {
    	$pages = Page::all(); 	
        return view('pages',
            ['pages'=>$pages,
             'languages'=>$this->languages]);
    		
    }

    /**
    * Validate and save new page
    *
    *Inesrt into equivalance zero value for the available languages 
    * For each language generate page and save
    * then update equivalance tbl with new ids
    *@param Request
    *@return redirect
    */
    public function store(Request $request) {

//unique:pages is the table name
   $this->validate($request, [
        'title' => 'required|unique:pages|max:255',
        'keywords' => 'required',
    ]);

        $equiv0 = new Equivalance;
        $columns = Schema::getColumnListing('equivalances');
        foreach ($columns as $colindex => $colang) {
            if ($colindex>2) {
                $equiv0->$colang   = 0;
            }
        }
        $equiv0->save();

        foreach ($this->languages as $lang){ 
            $page = new Page;
            $page->title    = $request->title;
            $page->content  = $request->content;
            $page->keywords = $request->keywords;
            $page->description = $request->description;
            $page->state    = $request->state;
            if ($request->hasFile('img_name')) {
                $path = $request->file('img_name')->store('images');
                $page->img_name = $path;
            }
            if ($request->hasFile('css_url')) {
                $path = $request->file('css_url')->storeAs('css', md5(\Carbon\Carbon::now()->toDateTimeString()).'.css');
                $page->css_url = $path;
            }
            if ($request->hasFile('js_url')) {
                $path = $request->file('js_url')->storeAs('js', md5(\Carbon\Carbon::now()->toDateTimeString()).'.js');
                $page->js_url = $path;
            }
        
            $page->js_code  = $request->js_code;
            $page->code_lang = $lang->code_lang;
            $page->page_layout = $request->page_layout;

            $page_title = preg_replace("/[^a-zA-Z0-9]/", "-", $request->title);

            $page->page_url = $lang->code_lang."/".$page_title;
            $idlang = $lang->code_lang;
            $page->user_id  = $request->user_id;
            $page->editor_id  = $request->user_id;

            $page->save();
            $idpage   = $page->id;
     
            //$equiv0->id: last id of Equivalance tbl
            $equiv0->where('id', $equiv0->id)->update([$idlang => $idpage]);
        }
       	return redirect()->action('PagesController@index',[]);
    }

    /**
    * Remove page
    *
    *Removing page in one language will cause the removal of sister pages in other language 
    *Removing blocks that belongs to the page and other pages
    *Then update equivalance tbl with new ids
    *Delete Equivalnace row that contain pages id
    *@param Page
    *@return redirect
    */
    public function remove(Page $page) {
        //Get Equivalance row by slecting the page id and lang 
        $rst = Equivalance::where($page->code_lang, $page->id)->first();
        //loop through the result and delete each page in the array result

        //  foreach ($rst as $key => $value) {
            //loop to get only the languages columns
            foreach ($this->languages as $lng) {
                //echo $rst[$lng->code_lang]."<br>";
                //destroy pages 
                $page_blocks = Page::find($rst[$lng->code_lang])->blocks;
                foreach ($page_blocks as $page_block) {
                    Block::destroy($page_block->id);
                }
                Page::destroy($rst[$lng->code_lang]);
            }               
        //}
        //destroy equivalance row
        Equivalance::destroy($rst->id);
        //for each page id delete block associated should be done automatically :onCascade delete
      //  $b->delete();
        return redirect()->action(
            'PagesController@index',[]
        );
    }


    public function show(Page $page) {
    	//1st option: pass no Type argument ($p), then use the line below 
    	//$page = Page::find($p);
    	//2nd option: pass Type of the argument(Page $p) and the variable has to be the same in Route
    	return $page;
    }

        public function edit(Page $page) {
            return view('pages_edit',['page'=>$page, 'languages'=>$this->languages]);
    }

    public function update(Request $request, Page $page) {

        $page_title = preg_replace("/[^a-zA-Z0-9]/", "-", $request->title);
        $page->title  = $request->title;
        $page->content  = $request->content;
        $page->keywords = $request->keywords;
        $page->description = $request->description;
        $page->state    = $request->state;
        $page->page_layout    = $request->page_layout;
        $page->state    = $request->state;

        if ($request->hasFile('img_name')) {
            $path = $request->file('img_name')->store('images');
            $page->img_name = $path;
        }
        if ($request->hasFile('css_url')) {
            $path = $request->file('css_url')->storeAs('css', md5(\Carbon\Carbon::now()->toDateTimeString()).'.css');
            $page->css_url = $path;
        }
        if ($request->hasFile('js_url')) {
            $path = $request->file('js_url')->storeAs('js', md5(\Carbon\Carbon::now()->toDateTimeString()).'.js');
            $page->js_url = $path;
        }

        $page->js_code  = $request->js_code;
        $page->code_lang = $request->code_lang;

        $page->page_url = $request->code_lang."/".$page_title;
        $page->editor_id       = $request->user_id;
        
        $page->save();
        return redirect()->action(
            'PagesController@index',[]
        );
    }


    public function removeFile($file, Page $page_id) {
        //in this version files are not deleted physically
        // $b = Page::find($page_id);
       //  $page_id->update(array('img_name' => Null));    
        if ($file == 'image') {
            $page_id->update(array('img_name' => Null)); 
            echo "img ";
         }
        elseif ($file == "css") {
        $pagedata= Page::find($page_id);
      //  dd(public_path());
            //unlink(asset('front/'.$pagedata->css_url));
            $page_id->update(array('css_url' => Null)); 
            echo "css ";
      //     dd($pagedata->css_url);
            }
            else {
             $page_id->update(array('js_url' => Null)); 
            echo "js ";
            }
        return " .ok";
    }

    public function showBlocks() {
        /*** for test purposes**/
        $comments = Page::find(53)->blocks;
        return $comments;
    }


}
