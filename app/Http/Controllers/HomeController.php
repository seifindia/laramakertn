<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Category;
use App\Page;
use App\Block;
use App\Company;
use App\Equivalance;
use Illuminate\Support\Facades\Mail;
use App\Mail\EnquiryNow;

class HomeController extends Controller
{
    

    public function __construct() {

      if (strpos (url()->full(), 'vw')!== false) {
          //(explode('.', $_SERVER['HTTP_HOST'])[0] == 'wmm'
          config(['database.default' => 'mysql_vw']);
          config(['app.name' => 'VadodaraWindows']);
      }
      elseif(strpos (url()->full(), 'wmm')!== false) {
          config(['database.default' => 'mysql_wmm']);
          config(['app.name' => 'Windowmaker Measure']);
      }
      else {
          config(['database.default' => 'mysql_wm']);
          config(['app.name' => 'Windowmaker']);
      }
    }
  
    
    public function index() {
    	return view('home');
    }
    
    /**
    * root is the index page of the site
    * the title must be index for the chosen home page
    * if there is no index page it will redirect to 404 page
    * otherwise it will load related blocks
    * $equivalance is used to get other index pages in other languages
    * @param void
    * @return view home
    * 
    */   
    public function root() {
      $c_page = Page::where('title', 'index')->first();
      if ($c_page !== null) {
        $blocks = Block::where('page_id', $c_page->id)->get();
        $equivalance = Equivalance::where($c_page->code_lang, $c_page->id)->first();
        return view('front.includes.home',compact('c_page', 'blocks', 'equivalance'));
      }
      else {
        abort(404);
      }
    }


    /**
    * Get page by id
    *
    * this method is called from MenuController
    * the urlwill be lang/pagetitle
    * @param id page
    * @return redirect to HomeController@internal
    * 
    */   
    public function pageById($id) {
      $c_page = Page::findOrFail($id);
       $page_title = preg_replace("/[^a-zA-Z0-9]/", "_", $c_page->title);

      return redirect()->action('HomeController@internal', ['lang'=>$c_page->code_lang, 'page'=>$page_title]);
    }


    /**
    * Get page by lang
    *
    * this method is called from MenuController
    * the urlwill be lang/pagetitle
    * @param page in requested lang
    * @return 
    * 
    */   
    public function pageByLang($equiv, $lang) {
         
       
          $equivalance = Equivalance::find($equiv); 
         // $c_page = Page::where($lang, $equivalance->$lang)->first();
           $c_page = Page::findOrFail($equivalance->$lang);


      return redirect()->action('HomeController@pageById', ['id'=>$c_page->id]);


/*
                $blocks = Block::where('page_id', $c_page->id)->get();
                //return $blocks; 
                return view('front.includes.home',compact('c_page', 'blocks', 'equivalance'));*/
         
    }




    /**
    * internalDefault will used if we have one language on the site
    * it will fetech the url of the given $page
    * compact c_page: page information, related blocks and equivalance ids 
    * $equivalance is used to get other index pages in other languages
    * @param page url
    * @return page without language slug
    * 
    */ 
    public function internalDefault($page) {
      /*$lang just for purpose test*/
      $lang = \App::getLocale();
      $c_page = Page::where('page_url', $page)->first();
        if ($c_page !== null) {
          $blocks = Block::where('page_id', $c_page->id)->get();
          $equivalance = Equivalance::where($lang, $c_page->id)->first();
          return view('front.includes.home',compact('c_page', 'blocks', 'equivalance'));
        }
        else {
         abort(404);
        }
    }


    /**
    * internal will used if we have one language on the site
    * it will fetech the url of the given page and lang
    * compact c_page: page information, related blocks and equivalance ids 
    * $equivalance is used to get other index pages in other languages
    * @param page & lang
    * @return page with language slug $lang.'/'.$page
    * 
    */ 
    public function internal($lang, $page) {
   
      $c_page = Page::where('page_url', $lang.'/'.$page)->first();
      if ($c_page !== null) {
        $blocks = Block::where('page_id', $c_page->id)->get();
        $equivalance = Equivalance::where($lang, $c_page->id)->first();
        return view('front.includes.home',compact('c_page', 'blocks', 'equivalance'));
      }
      else {
        abort(404);
      }
    }
 

    /**
    * Search by word from all blocks
    * lang condition should be added
    * @param string word
    * @return view result_search
    * 
    */ 
     public function search_for(Request $request, Block $block) {
      $word = $request->Keywords;
      if (!empty($word)) {
        $results = Block::where('sub_content', 'like', '%' . $word. '%')->get();
        return view('result_search', compact('results'));
      }
      else {
        return redirect()->action('HomeController@root');
      }
    }


    /**
    * Send Email when any footer contact form is filled
    * the send method shoudld receive a Mailable instance (app/Mail/...)
    * the to method receive the reception email (can be configured/created to be in config/mail/)
    * the from email and name can be found in  config/mail/
    * @param request
    * @return redirection
    */

    public function contactUs(Request $request) {
       Mail::to("seifallah.ba@gmail.com")->send(new EnquiryNow($request));
       return redirect()->action('HomeController@root',[]);
    }

    /**
    * Logout and destroy xurrent user session
    * @param void
    * @return redirection to root
    */

    public function logout() {
      session()->flush();
      Auth::logout();
      return redirect()->action('HomeController@root',[]);
    }
}
