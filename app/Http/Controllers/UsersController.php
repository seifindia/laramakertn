<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;


class UsersController extends MainBaseController
{
    //
    public function __construct() {
           parent::__construct();
        }

        public function index() {
        	$users = User::all();
        	return view('users', ['users'=>$users]);
        }

        /**
        *Add new user
        *
        *From Dashboard, add new user
        *
        */
    public function store(Request $request) {
    		$user = new User;
    		$user->name = $request->name;
    		$user->email = $request->email;
    		$user->password = bcrypt($request->password);

    		$user->save();

    	return redirect()
    			->action('UsersController@index', []);
    }


    public function profile($id) {
        $user = User::find($id);
        return view('profile')->with(['user'=>$user]);
    }

    public function update(Request $request) {
            $user = User::find($request->id);
            $user->name = $request->name;
            $user->email = $request->email;
            if ($request->password != null) {
                $user->password = bcrypt($request->password);
            }

            $user->save();

        return redirect()
                ->action('PagesController@index', []);
    }

    
}
