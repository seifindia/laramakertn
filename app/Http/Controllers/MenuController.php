<?php

namespace App\Http\Controllers;

use App\Menu;
use App\MenuType;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    
    /**
     * Store a newly created Menu Type.
     *
     * @return \Illuminate\Http\Response
     */
    public function storeMenuType(Request $request)
    {
        $menutype = new MenuType;
        $menutype->name = $request->menu_title;
        $menutype->save();
         return redirect()->action('ConfigController@index', []);
    }
    /**
     * Store a newly created Menu item.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $menu = new Menu;
        $menu->title = $request->menu_title;
        $menu->lang = $request->menu_lang;
        if ($request->menu_link_id) {
            $menu->link = $request->menu_link_id;
        }
        else {

            $menu->link = 0;
            $menu->external_link = $request->menu_link_external;
        }
        $menu->parent = $request->menu_parent;
        $menu->icon = $request->menu_icon;
        $menu->order_menu = $request->menu_tiz;
        $menu->type = $request->menu_type;
        $menu->save();
         return redirect()->action('ConfigController@index', []);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Menu $menu)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {   $menu->delete();
        return  redirect()->action('ConfigController@index', []);
        //$m = Menu::find($menu)
    }
}
