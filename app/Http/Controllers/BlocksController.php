<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Block;
use App\Page;
use App\Language;
use App\Equivalance;
use Illuminate\Support\Facades\Auth;

class BlocksController extends MainBaseController
{
    
    private $pages;


    /**
    * Load languages from MainBaseController
    *
    * Initliaze pages so it will be accessible for 
    * all the methods within this controller
    * make sure to always declare the varname as private
    * then access it with $this->varname
    *
    * "The varname in Parent Controller must be protected"
    * @return array of pages
    */
    public function __construct() {
        parent::__construct();
        $this->pages = Page::all();
    }


    /**
    * Index is the default method will be called
    *
    * Load all blocks
    * Get all pages inilized in constructor 
    * Load all languages fro; Parent Controller
    * @return array of blocks, pages, languages
    */

    public function index() {
    	$blocks = Block::all();
    	return view('blocks',
            ['blocks'=>$blocks,
             'pages'=>$this->pages,
             'languages'=>$this->languages]);
    }

    /**
    * Store is save new block
    *
    * This method is handled via ajax
    * 1.Find first language, return type Object
    * 2.Get the id of Equivalance tbl for the selected page to get ids of pages in all languages
    * 3.foreach available language, 
    * 3.1 create new block 
    * 3.2 idlang (code_lang) will be used to get the page id of current boucled language
    * 3.3 Assign variables
    * 3.4 $equiv->$idlang is to get page_id
    * 3.5 $orderlist contains blocks ids
    * 3.6 save the current block with order eauql to his id (new_index)
    * 3.7 then udpate sort_order of each block
    * 4. save new block if it's the only one with sort_order equal to 1 
    * 5. empty $idlang 
    * 
    * 
    * NB: Blocks in different language will be saved in order of their id,
    * when one block is edited , the order will in sequence 
    * @return redirect to blocks views
    */

    public function store(Request $request) {        
        $pageinfo = Page::find($_POST['page_id']);
        $equiv = Equivalance::Where($pageinfo->code_lang, $pageinfo->id)->first();
        foreach ($this->languages as $lang){   		
            $block = new Block;
            $idlang = $lang->code_lang;

        	$block->sub_title 		= $_POST['sub_title'];
            $block->sub_content     = $_POST['sub_content'];
            $block->block_layout    = $_POST['block_style'];
            $block->page_id         = $equiv->$idlang;
        	$block->code_lang 		= $idlang;
            $block->user_id         = $request->user_id;
            $block->editor_id       = $request->user_id;

            $orderlist = explode(',', $_POST['order']);

            if (sizeof($orderlist) > 1) {
                /*
                * save current block with sort_order eaqul to his id
                */
                $block->sort_order      = $_POST['sort_order'];

                $block->save();

                /*
                * update sort_order of blocks where $k is ascending order
                */
                foreach ($orderlist as $k=>$order) {
                $k++;
                Block::where('id', $order)->update(['sort_order' => $k]);
                echo '*** '.$idlang.' **** UPDATE_blocks_SET_sort_order ' . $k . ' WHERE_id= ' . $order . '\n';
                }  
            }
            else {
             /*
                * save current block with sort_order eaqul 1
                */
                $block->sort_order      = '1';

                $block->save();
            }
       	    $idlang = "";
        }//#endforeach
dd();
		return redirect()->action(
		    'BlocksController@index',[]
		);
    }

    public function remove(Block $block) {
        $b = Block::find($block->id);
        $b->delete();
        return redirect()->action(
            'BlocksController@index',[]
        );
    }

    public function show(Block $block) {
    	return $block;
    }

    public function edit(Block $block) {
        return view('blocks_edit',[
            'block'=>$block,
            'pages'=>$this->pages,
            'languages'=>$this->languages
            ]);
    }

        /**
        * Udapte Current block
        *
        * Alternative to save a model
        * pass 2 args to the method (Request $request , Block $block),
        *  or get instance of your current Model
        * $block->update($request->all());
        * then redirect or back ...
        */

    public function update(Block $block) {

       
        $block = Block::find($_POST['_id']);

        $block->sub_title       = $_POST['sub_title'];
        $block->sub_content     = $_POST['sub_content'];
        $block->block_layout    = $_POST['block_style'];
        $block->page_id         = $_POST['page_id'];
        $block->code_lang       = $_POST['code_lang'];
        $block->editor_id       = $request->user_id;

        $orderlist = explode(',', $_POST['order']);

        /**
        * update current block then update the order of all blocks
        * 
        */ 
        $block->save();

        #update
        foreach ($orderlist as $k=>$order) {
            $k++;
            //UPDATE blocks SET sort_order = $k WHERE id = $order
            Block::where('id', $order)->update(['sort_order' => $k]);
        }  

    }

    public function parentPage() {
         return $this->belongsTo('App\Page');
    }
    
    public function getByPage($page_id) {
        $blocs = Block::where('page_id', $page_id)->orderBy('sort_order')->get();
        return $blocs;
    }


    /**
    * getMaxIdBloc to get last block id then increment by 1
    * $maxidblock will be used as an id for the new unsaved block
    *
    */
    public function getMaxIdBloc() {
        $nbr_blocks = \DB::table('blocks')->count();
        if ($nbr_blocks > 0) {
            $maxidblock = Block::find(\DB::table('blocks')->max('id'));
            $maxidblock = $maxidblock->id+1;
            return $maxidblock;
        }
        else {
            $maxidblock = 1;
            return $maxidblock;
        }
      
    }


}
