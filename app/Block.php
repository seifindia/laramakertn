<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    
    	//fields to change from the form: MassAssignment
    protected $fillable = [
    	'sub_title', 
    	'sub_content',
    	'page_id',
        'code_lang'
        ];
       /* public function category() {
        	return $this->belongsTo(Category::class);
        }
    */
            public function pagename($id) {
                $page =     Page::where('id', '=' , $id)->select('title')->first();
                //$page = Page::find($id);
                //trying-to-get-property-of-non-object
                $title = $page['title'];
                return $title;
        }
    

}
