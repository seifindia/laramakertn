<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
	//fields to change from the form: MassAssignment
    protected $fillable = [
    	'title', 
    	'content',
    	'keywords',
        'description',
        'page_layout',
    	'state',
    	'img_name',
    	'css_url',
    	'js_url',
    	'js_code',
    	'code_lang'
    ];

    public function blocks() {
           return $this->hasMany('App\Block');
           //return $this->hasMany(Block::class);

    }

    
}
