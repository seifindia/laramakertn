<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    //fields to change from the form: MassAssignment
    protected $fillable = [
    	'full_name',
    	'short_name', 
    	'email',
    	'favicon',
        'url',
        'adress',
    	'phone',
    	'description',
    	'youtube',
    	'facebook',
    	'twitter',
    	'linkedin'
    ];
}
