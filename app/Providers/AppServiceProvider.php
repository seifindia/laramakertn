<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */ 
    public function boot()
    {
        /** to resolve migration problem*/
         Schema::defaultStringLength(191);
  
    /**
     * Load and Bind Vars to viewa.
     *
     * pass the names of views through view->composer array
     * then attach the variables
     *
     * @return view names of views
     */
         view()->composer(['header', 'footer', 'configs', 'front.includes.home',  'front.includes.nav-menu-wm', 'result_search'], function($view) {
            $view->with(['comp' => \App\Company::first(), 'langs' => \App\Language::all()]);
         });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        
    }
}
