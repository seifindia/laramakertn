<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('lang');
            $table->smallInteger('link'); //id of page
            $table->string('external_link')->nullable(); 
            $table->smallInteger('parent'); //id of parent menu
            $table->string('icon')->nullable(); //can be image also
            $table->smallInteger('order_menu')->nullable();
            $table->string('type'); //main, footer, sidebar menu depensing on position
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
