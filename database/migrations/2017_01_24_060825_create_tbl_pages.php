<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblPages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('content')->nullable();
            $table->string('keywords')->nullable();
            $table->string('description')->nullable();
            $table->smallInteger('state');
            $table->string('img_name')->nullable();
            $table->string('css_url')->nullable();
            $table->string('js_url')->nullable();
            $table->text('js_code')->nullable();
            $table->string('page_url');
            $table->char('code_lang', 4);
            $table->smallInteger('user_id');
            $table->smallInteger('editor_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
