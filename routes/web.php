<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/


Auth::routes();

/*Dashboard */

Route::get('manager','PagesController@index');
Route::get('home','HomeController@index');
Route::get('/','HomeController@root');
Route::get('manager/logout','HomeController@logout');
Route::get('manager/pages', 'PagesController@index');
Route::post('manager/pages', 'PagesController@store');

Route::get('manager/blocks', 'BlocksController@index');
Route::post('manager/blocks', 'BlocksController@store');

Route::get('manager/maxidbloc', 'BlocksController@getMaxIdBloc');

Route::delete('manager/blocks/{block}', 'BlocksController@remove');
Route::patch('manager/blocks/update/{block}', 'BlocksController@update2');
Route::post('manager/blocks/update', 'BlocksController@update');

Route::get('manager/storethem', 'PagesController@showForm');
Route::post('manager/storethem', function(){
	request()->file('ahla')->storeAS('ahlas', 'test.js');return back();
});


Route::get('manager/users', 'UsersController@index');
Route::post('manager/users', 'UsersController@store');
Route::get('manager/users/profile/{id}', 'UsersController@profile');
Route::post('manager/users/update', 'UsersController@update');


Route::get('manager/categories', 'CategoryController@index');
Route::post('manager/categories', 'CategoryController@store');

Route::get('manager/media', 'MediaController@index');
Route::get('manager/mediaselect', 'MediaController@listImages');
Route::post('manager/media', 'MediaController@upload');
Route::post('manager/media/delete', 'MediaController@remove');

Route::get('manager/languages', 'LanguagesController@index');
//Route::get('manager/languages', 'LanguagesController@index')->middleware('auth');



Route::post('manager/languages', 'LanguagesController@store');
Route::delete('manager/languages/{lang}', 'LanguagesController@remove');

Route::get('manager/config', 'ConfigController@index');
Route::post('manager/config', 'ConfigController@saveFile');
Route::get('manager/config/backup', 'ConfigController@backupdb');
Route::post('manager/config/company', 'ConfigController@storeCompany');
Route::patch('manager/config/company', 'ConfigController@UpdateCompany');
Route::get('manager/config/send', 'ConfigController@sendEmail');
Route::post('manager/config/menutype', 'MenuController@storeMenuType');
Route::post('manager/config/menu', 'MenuController@store');
Route::delete('manager/config/menu/{menu}', 'MenuController@destroy');

//Be carefeull any route with variable must be at the end
Route::get('manager/single/{id}', 'TestController@showUser');
 
Route::get('manager/pages/{page}', 'PagesController@show');
Route::get('manager/blocks/{block}', 'BlocksController@show');
Route::get('manager/blocks/edit/{block}', 'BlocksController@edit');
Route::get('manager/categories/{category}', 'CategoryController@show');

Route::get('manager/test', function(){

//	abort(500);
/*
$pagelang = \App\Page::find(18);
return $pagelang;
$equiv = \App\Equivalance::Where($pagelang->code_lang, 18)->first();

return  $equiv;*/

	$counts = \DB::table('languages')
             ->select(\DB::raw(' count(pages.id) as nbrs, name'))
            ->leftJoin('pages', 'languages.code_lang', '=', 'pages.code_lang')
            ->groupBy('languages.name')
            ->get();
	return $counts;

});


Route::group(['middleware' => ['web']], function () {

Route::get('manager/pages/edit/{page}', 'PagesController@edit');
Route::delete('manager/pages/{page}', 'PagesController@remove');
Route::patch('manager/pages/update/{page}', 'PagesController@update');
Route::get('manager/page/{file}/{page_id}', 'PagesController@removeFile');

});


Route::get('manager/blocks_page/{page_id}', 'BlocksController@getByPage');

//Route::get('manager/compile', 'HomeController@compilerBlade');
//Route::get('front/inherit/', 'AllMenuController@index');
Route::get('contact/send', 'HomeController@contactUs');
Route::get('search',  'HomeController@search_for');
Route::get('subscribe',  'Auth\RegisterController@showRegistrationForm');

/* FRONT */
/*Route::get('{equiv}/{lang}/lang', 'HomeController@pageByLang');
Route::get('{id}', 'HomeController@pageById');
Route::get('/{page}', 'HomeController@internalDefault');
Route::get('{lang}/{page}', 'HomeController@internal');*/