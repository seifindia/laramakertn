@extends ('admin_tmpl')
	@section ('content')
{{--------		<h2>Hola el mundo...</h2>
		<p>UnEscaped data {{ $nom }} !!</p>
		<p>Escaped data {!! $nom !!}</p>

This is a block comment in laravel

		@if (count($people))
			<h2>Foreach</h2>
			<ul>
				@foreach($people as $person)
					<li>{{ $person }}</li>
				@endforeach
			</ul>
		@else
			<h2>We have nobody</h2>
		@endif
--------}}
<h3>List  of users</h3>
<ul>
@foreach ($users as $user)
<li> <a href="single/{{$user->id}} ">{{ $user->name }} | {{ $user->email }} </a></li>
<!-- <li> <a href="{{action('TestController@showUser',[$user->id])}} ">{{ $user->name }} | {{ $user->email }} </a></li> -->
<!-- <li> <a href="{{ url('single', $user->id) }} ">{{ $user->name }} | {{ $user->email }} </a></li> -->
@endforeach	

</ul>
	@endsection