@extends ('admin_tmpl')
	@section('mystyles')
	  <link rel="stylesheet" href="{{asset("/node_modules/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css")}}">
	  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset("/node_modules/admin-lte/plugins/datatables/dataTables.bootstrap.css")}}">
    <!-- Select2 -->
  <link rel="stylesheet" href="{{asset("/node_modules/admin-lte/plugins/select2/select2.min.css")}}">
	@endsection

	@section('page_title', 'Users')
	@section ('content')
	<div class="row" id="users-form">
		<form class="form-horizontal" method="POST" action="users">
        <div class="col-md-12">
          <div class="box box-info collapsed-box">
            <div class="box-header">
              <h3 class="box-title">Add User</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body pad">
        		<div class="col-md-12">
					<div class="form-group">
			          	<input class="form-control" type="text" placeholder="Username" name="name">
			        </div>
		            <div class="form-group">
		                <input class="form-control" type="email" placeholder="email" name="email">         	
		            </div>
		            <div class="form-group">
		                <input class="form-control" type="password" placeholder="password" name="password">         	
		            </div>
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="form-group">
	              			 <button type="submit" class="btn btn-info">Save</button>
	            	</div>
            	</div>
        	</div>
          	</div>
          <!-- /.box -->
		</div>

		</form>
	</div> <!-- #row -->
	<div class="row" id="users-list">
		<div class="col-md-12">
			<div class="box">
            <div class="box-header">
              <h3 class="box-title">Users List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="pages-tab" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Added at</th>
                  <th><i class="fa fa-edit"></i></th>
                  <th><i class="fa fa-trash"></i></th>
                  <th><i class="fa fa-send"></i></th>
                </tr>
                </thead>
                <tbody>
              @if(empty($users))
 					#No data
              @else
                @foreach ($users as $user)
	                <tr>
	                  <td># {{ $user->id }}</td>
	                  <td>{{ $user->name }}</td>
	                  <td>{{$user->email}}</td>
	                  <td>{{$user->created_at}}</td>
	                  <td><a href=""><i class="fa fa-edit"></i></a></td>
	                  <td><a href="users/{$user->id}/bye"><i class="fa fa-trash"></i></a></td>
	                  <td><a href=""><i class="fa fa-send"></i></a></td>
	                </tr>
				@endforeach	
              @endif
                </tbody>
                <tfoot>
                <tr>
                   <th>#</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Added at</th>
                  <th>edit</th>
                  <th>Trash</th>
                  <th>Resend Password</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
        </div>
          <!-- /.box -->
		</div>
	</div>

		@section ('myscripts')
			<!-- CK Editor -->
			<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
			<!-- Bootstrap WYSIHTML5 -->
			<script src="{{asset("/node_modules/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js")}}"></script>
			<!-- DataTables -->
<script src="{{asset("/node_modules/admin-lte/plugins/datatables/jquery.dataTables.min.js")}}"></script>
<script src="{{asset("/node_modules/admin-lte/plugins/datatables/dataTables.bootstrap.min.js")}}"></script>
<!-- SlimScroll -->
<script src="{{asset("/node_modules/admin-lte/plugins/slimScroll/jquery.slimscroll.min.js")}}"></script>
<!-- FastClick -->
<script src="{{asset("/node_modules/admin-lte/plugins/fastclick/fastclick.js")}}"></script>
<!-- Select2 -->
<script src="{{asset("/node_modules/admin-lte/plugins/select2/select2.full.min.js")}}"></script>
			<script>
			  $(function () {
			    // Replace the <textarea id="editor1"> with a CKEditor
			    // instance, using default configuration.
			 //   CKEDITOR.replace('editor1');
			    //bootstrap WYSIHTML5 - text editor
			    $(".textarea").wysihtml5();
				$('#pages-tab').DataTable(); 
				$(".select2").select2();
			  });
			</script>
		@endsection

	@endsection