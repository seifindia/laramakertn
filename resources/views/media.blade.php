
    @extends ('admin_tmpl')
    	@section('mystyles')
    	@endsection
    	@section('page_title', 'Media')
	@section ('content')
		<div class="row" id="media-from">
			<div class="col-md-12">
				<div class="box box-info">
	            <div class="box-header">
	              <h3 class="box-title">Upload Files <!-- <button type="button" class="btn btn-info" id="copypath">Copy</button> --></h3>
	            </div>
	            <!-- /.box-header -->
	            <div class="box-body pad">
	        		<div class="col-md-12">
						<form class="form-horizontal" method="POST" action="media" enctype="multipart/form-data">
							<div class="form-group">
					          	<input class="form-control" type="file" name="mediafile" multiple="multiple">
					        </div>
							<div class="form-group">
								<select class="form-control select2" name="folder">
									<option value="images">Images</option>
									<option value="pdf">PDF</option>
									<option value="assets">Others</option>
								</select>
												        </div>
							<input type="hidden" name="_token" value="{{ csrf_token() }}"/>
							<div class="form-group">
							 	<button type="submit" class="btn btn-info">Upload</button>
				            </div>
						</form>
	        		</div>
	          	</div>
	          	<!-- /.boxbody -->
		  	</div><!-- box-info -->
			</div>
		</div>
		<div class="row" id="media-list">
		<div class="col-md-12">
		<div class="box box-info">
				<div class="box-header">
		           <h3 class="box-title">Media List</h3>
		        </div>

	            <!-- /.box-header -->

	            <div class="box-body pad">
<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#images">Images</a></li>
  <li><a data-toggle="tab" href="#pdf">PDF</a></li>
  <li><a data-toggle="tab" href="#css">CSS</a></li>
  <li><a data-toggle="tab" href="#js">Js</a></li>
  <li><a data-toggle="tab" href="#others">Others</a></li>
</ul>

<div class="tab-content">
  <div id="images" class="tab-pane fade in active">
    <h3>{{count($imagesfiles)}} Images files</h3>
		<div class="row">
		@foreach ($imagesfiles as $imgfile)
			<div class="col-md-2">
	        		<img src="{{asset($imgfile->getPathname())}}" class="img-responsive img-thumbnail">
				<form action="media/delete" method="POST">
					<input type="hidden" name="filepath" value="{{$imgfile->getPathname()}}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}"/>
						<button type="submit"><i class="fa fa-trash"></i></button>
					</form>
	        </div>
		@endforeach
  			
  		</div>
  </div>
  <div id="pdf" class="tab-pane fade">
 <h3>{{count($pdfiles)}} Pdf files</h3>
    <ul>
		@foreach ($pdfiles as $pdfile)
			<li>
			<form action="media/delete" method="POST">
					<input type="hidden" name="filepath" value="{{$pdfile->getPathname()}}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}"/>
						<button type="submit"><i class="fa fa-trash"></i></button>
					</form>
				<a target="_blank" href="{{asset($pdfile->getPathname())}}"><i class="fa fa-eye"></i> | {{$pdfile->getFilename()}}</a>
			</li>	        		
		@endforeach  </div>
  <div id="css" class="tab-pane fade">
    <h3>{{count($cssfiles)}} Css files</h3>
    	<!-- <a href="$cssfile->getPathname()"><i class="fa fa-trash"></i> | $cssfile->getFilename ()</a> -->
    <ul>
		@foreach ($cssfiles as $cssfile)
				<li>
				<form action="media/delete" method="POST">
					<input type="hidden" name="filepath" value="{{$cssfile->getPathname()}}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}"/>
						<button type="submit"><i class="fa fa-trash"></i></button>
					</form>
					<a target="_blank" href="{{asset($cssfile->getPathname())}}"><i class="fa fa-eye"></i> | {{$cssfile->getFilename()}}</a>
				</li>	        		
		@endforeach
	</ul>
  </div>
  <div id="js" class="tab-pane fade">
  <h3>{{count($jsfiles)}} js files</h3>
    <ul>
		@foreach ($jsfiles as $jsfile)
				<li>
				<form action="media/delete" method="POST">
					<input type="hidden" name="filepath" value="{{$jsfile->getPathname()}}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}"/>
						<button type="submit"><i class="fa fa-eye"></i></button>
					</form>
					<a target="_blank" href="{{asset($jsfile->getPathname())}}"><i class="fa fa-trash"></i> | {{$jsfile->getFilename()}}</a>
				</li>	        		
		@endforeach
	</ul>
  </div>
  <div id="others" class="tab-pane fade">
     <h3>{{count($assetfiles)}} Others files</h3>
    <ul>
		@foreach ($assetfiles as $assetfile)
				<li>
					<form action="media/delete" method="POST">
					<input type="hidden" name="filepath" value="{{$assetfile->getPathname()}}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}"/>
						<button type="submit"><i class="fa fa-trash"></i></button>
					</form>
					<a target="_blank" href="{{asset($assetfile->getPathname())}}">{{$assetfile->getFilename()}}</a>

				<!-- 	<a href="media/delete/{{$assetfile->getPathname()}}"> | {{$assetfile->getFilename()}}</a> -->
				</li>	        		
		@endforeach
	</ul>
  </div>
</div>


				 <!-- @for ($i = 0; $i < 10; $i++)
    	       		<div class="col-md-2">
	        		<img src="{{asset("/node_modules/admin-lte/dist/img/avatar.png")}}" alt="..." class="img-responsive img-thumbnail">
	        		<p class="text-center"><a href="">FileName | <i class="fa fa-trash"></i> Remove</a></p>
	        		</div>
				@endfor -->




	          	</div>
	          	<!-- /.boxbody -->
		  	</div><!-- box-info -->
		</div>
		</div>
		@section ('myscripts')
			<script type="text/javascript">
			$("#copypath").click(function(){
				$("input#cke_123_textInput").val('http://hlif.com');
				alert('e');
			});
			</script>
		@endsection
	@endsection
