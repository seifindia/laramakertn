
	@extends ('admin_tmpl')
	@section('mystyles')
	  <link rel="stylesheet" href="{{asset("/node_modules/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css")}}">
	  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset("/node_modules/admin-lte/plugins/datatables/dataTables.bootstrap.css")}}">
    <!-- Select2 -->
  <link rel="stylesheet" href="{{asset("/node_modules/admin-lte/plugins/select2/select2.min.css")}}">
    	  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style>
 #sortable { list-style-type: none; margin: 0; padding: 5px;  }
 li.ui-state-default {
    margin-bottom: 8px;
}

  </style>

	@endsection

	@section('page_title', 'Edit Block')
	@section ('content')
	<div class="row" id="edit-form">
	<div class="col-md-12">
	<form class="form-horizontal" method="POST" action="">
	<input type="hidden" name="_token" value="{{ csrf_token() }}"/>
	<div class="col-md-12">
	<div class="box box-info">
	<div class="box-header">
	<h3 class="box-title">Update '{{$block->sub_title}}'</h3>
	<div class="box-tools pull-right">
	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
	</button>
	</div>
	<!-- /.box-tools -->
	</div>
	<!-- /.box-header -->
	<div class="box-body pad">
		<div class="form-group">
		<div class="">
			<div class="col-md-8">
				<input class="form-control" type="text" placeholder="Sub Title" name="sub_title" value="{{$block->sub_title}}">
			</div>
			<div class="col-md-4">
				<select class="form-control select2" requied="requied" style="width: 80%;" name="code_lang">
				  @foreach ($languages as $lang)
				    <option value="{{$lang->code_lang}}" @if($lang->code_lang == $block->code_lang) selected="selected" @endif>{{$lang->name}}</option>
				  @endforeach
				</select>
			</div>
		</div>
		</div>
	<div class="col-md-12">
	<div class="form-group">

	<textarea id="editor1" class="textarea col-md-12" name="sub_content" rows="5"  placeholder="This is my textarea to be replaced with page content.">
		{{$block->sub_content}}
	</textarea>                  	
	</div>
	<div class="form-group">
	<div class="row">
		<div class="col-md-8">
		<label>Page</label><br>
		<select class="form-control select2" id="select_page" required="requried" style="width: 90%;" name="page_id">
		<option value="0" selected="selected">None</option>
		@foreach ($pages as $cat)
		<option value="{{$cat->id}}" @if($cat->id == $block->page_id) selected @endif>{{$cat->title}}-{{$cat->code_lang}}</option>
		@endforeach	
		</select>

		<p>
			<ul id="sortable"></ul>
		</p>
		</div>
		<div class="col-md-4">
			<label>Block style: <span id="refresh-me"><i class="fa fa-refresh"></i></span></label>
				 <select class="form-control select2" id="block_style" data-placeholder="Select Style" required="required"
		    style="width: 100%;" name="block_style"></select>
		</div>


	</div>

	</div>			

	<input id="_id" type="hidden" value="{{$block->id}}">
    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
	<input name="_method" type="hidden" value="PATCH">
	<div class="form-group">
	<button type="button" onclick='saveOrder();' class="btn btn-info">Save</button>
	</div>
	</div>
	</div>
	<!-- /.boxbody -->
	</div><!-- box-info -->
	</div>
	</form>
	</div>
	</div>
	@section ('myscripts')
			<!-- CK Editor -->
			<script src="{{asset("/node_modules/admin-lte/plugins/ck/ckeditor.js")}}"></script>
			<!-- Bootstrap WYSIHTML5 -->
			<script src="{{asset("/node_modules/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js")}}"></script>

<!-- SlimScroll -->
<script src="{{asset("/node_modules/admin-lte/plugins/slimScroll/jquery.slimscroll.min.js")}}"></script>
<!-- FastClick -->
<script src="{{asset("/node_modules/admin-lte/plugins/fastclick/fastclick.js")}}"></script>
<!-- Select2 -->
<script src="{{asset("/node_modules/admin-lte/plugins/select2/select2.full.min.js")}}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
			<script>
			  $(function () {


			    // Replace the <textarea id="editor1"> with a CKEditor
			    // instance, using default configuration.
			 //   CKEDITOR.replace('editor1');
			    //bootstrap WYSIHTML5 - text editor
			 

			     CKEDITOR.replace('editor1');
			     CKEDITOR.config.startupMode = 'source';
   CKEDITOR.config.filebrowserBrowseUrl = '../../mediaselect';

			     	 $( "#sortable" ).sortable();
    			 $( "#sortable" ).disableSelection();
			    

    $.get('../../blocks_page'+"/"+{{$block->page_id}})
        .success(function(data) {
			 console.log("result length"+data.length);
			 console.log("result data"+data);

			if (data.length > 0) {
				$(data).each(function(index, value) 
				{
				  	console.log('index'+index);
				    $("#sortable").append("<li class='ui-state-default'  data-article-id='"+data[index].id+"'><span class='ui-icon ui-icon-arrowthick-2-n-s'></span>"+data[index].sub_title+"</li>");
				});
			}
          
        })
        .error(function(data) { 
            //alert('Error: ' + data); 
        }); 


			if ({{$block->block_layout}} == 1) {
			//full page
			$("#block_style").html("<option value='1'>Full</option><option value='2'>Centred</option>");
				            	
			}
			else if({{$block->block_layout}} == 2){
			//centred
					$("#block_style").html("<option value='2'>Centred</option>");
			}
			else{
			//right or left sidebar. 3:rightsidebar | 4:leftsidebar
				$("#block_style").html("<option value="+{{$block->block_layout}}+">Sidebar</option><option value='2'>Centred</option>");
			}


			     //$(".editor1").wysihtml5();
				$(".select2").select2();

$("#refresh-me").click(function(){
getPageById({{$block->page_id}});
});
  $("#select_page").change(function(){
  $("#sortable").empty();
    pageID = $(this).val();
    console.log("pageID"+pageID);
    j_sub_title = $("#sub_title").val();
    $.get('../../blocks_page/'+pageID)
        .success(function(data) {
			 console.log("result length"+data.length);
			 console.log("result data"+data);

			if (data.length > 0) {
				$(data).each(function(index, value) 
				{
				  	console.log('index'+index);
				    $("#sortable").append("<li class='ui-state-default'  data-article-id='"+data[index].id+"'><span class='ui-icon ui-icon-arrowthick-2-n-s'></span>"+data[index].sub_title+"</li>");
				//new_index++;
				});

			 	$("#sortable").append("<li class='ui-state-default'  data-article-id='{{$block->id}}'><span class='ui-icon ui-icon-arrowthick-2-n-s'></span>{{$block->sub_title}}</li>");
//			 	$("#_neworder").val(new_index);
			}
			else {

				$("#sortable").append("<li class='ui-state-default'  data-article-id='1'><span class='ui-icon ui-icon-arrowthick-2-n-s'></span>{{$block->sub_title}}</li>");
			}

          
        })
        .error(function(data) { 
            //alert('Error: ' + data); 
        }); 


getPageById(pageID);
  });






				var nbr = 0;
				setTimeout(function(){ 
						
				$("#cke_33").click(function(){
					nbr++;
					if (nbr % 2 === 0) { 
					$("#notif").addClass("hidden"); }
   					else { $("#notif").removeClass("hidden");}
				});


					 }, 1000);

			});

			  function getPageById(pageID) {
			  	        $.get('../../pages/'+pageID)
        .success(function(data) {
			 console.log("result show page data"+data.page_layout);
			if (data.page_layout == 1) {
			//full page
			$("#block_style").html("<option value='1'>Full</option><option value='2'>Centred</option>");
				            	
			}
			else if(data.page_layout == 2){
			//centred
					$("#block_style").html("<option value='2'>Centred</option>");
			}
			else{
			//right or left sidebar. 3:rightsidebar | 4:leftsidebar
				$("#block_style").html("<option value="+data.page_layout+">Sidebar</option><option value='2'>Centred</option>");
			}

          
        })
        .error(function(data) { 
           alert('Error: ' + data); 
        }); 

			  }

function saveOrder() {
    var articleorder = "";
    var _id, sub_title, sub_content,page_id, sort_order, code_lang ="";
    
    sub_title 	= $('input[name=sub_title]').val();
    _id 	= $('input#_id').val();
    sub_content = CKEDITOR.instances.editor1.getData();
    page_id 	= $('select[name=page_id]').val();
    block_style 	= $('select[name=block_style]').val();
    _token 	= $('input[name=_token]').val();
    _method 	= "POST";
    //sort_order 	= $("#_neworder").val();
    code_lang 	= $('select[name=code_lang]').val();

    $("#sortable li").each(function(i) {
        if (articleorder=='')
            articleorder = $(this).attr('data-article-id');
        else
            articleorder += "," + $(this).attr('data-article-id');
    });
            //articleorder now contains a comma separated list of the ID's of the articles in the correct order.
            console.log("bfefore send: block id -  "+_id+" - "+code_lang);
    $.post('../update', { _id:_id, order: articleorder, sub_title:sub_title, sub_content:sub_content, page_id:page_id,  block_style:block_style, _token:_token, code_lang:code_lang })
        .success(function(data) {
            //alert('saved');
            window.location.replace("../../blocks");
            console.log("result ",data);
        })
        .error(function(data) { 
            //alert('Error: ' , data); 
        }); 
}

			</script>
		@endsection

	@endsection