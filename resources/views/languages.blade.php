
@extends ('admin_tmpl')
@section('page_title', 'Languages & Keywords')
@section ('content')
<div class="row" id="languages-form">
	<div class="col-md-6">
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Add Language</h3>
			</div>
			<div class="box-body pad">
				<form method="POST" action="languages" class="" enctype="multipart/form-data">
					<div class="form-group">
						<input type="text" required="required" name="name" class="form-control" placeholder="Language name">
					</div>
					<div class="form-group">
						<input type="text" name="code_lang" required="required" class="form-control" placeholder="Language Abreviation">				
					</div>
					<div class="form-group">
						<label>Language Flag</label>
						<br>
						<div class="input-group">
						<input type="file" name="flag_img" placeholder="Flag image" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<button type="submit" class="btn btn-info">Save</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="box box-success">
			<div class="box-header">
				<h3 class="box-title">Languages List</h3>
			</div>
			<div class="box-body pad" id="lang-box">
			<ul class="list-group">
				@foreach ($languages as $lang)
					<li class="list-group-item">
					 <form method="POST" action="languages/{{ $lang->id }}" class="delpage" style="display: inline;
    float: right;margin-top: 10px;">
	                   {{ csrf_field() }}
	                  <input name="_method" type="hidden" value="DELETE">
	                  	<button type="button" class="delbtn">
	                  		<i class="fa fa-trash"></i>
	                  	</button>
	                  </form>

	                  <img class="img-thumbnail" src=" @if($lang->flag_img != NULL)
{{asset("/front/$lang->flag_img")}} @else https://upload.wikimedia.org/wikipedia/en/e/e3/CS_Hammam-Lif-Logo-2008.png @endif " width="50" height="50"> {{$lang->name}} </li>
				@endforeach
			</ul>
			</div>
		</div>
	</div>
<!-- 	<div class="col-md-4">
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Add Keywords</h3>
			</div>
			<div class="box-body pad">
				<form method="POST" action="" class="">
					@for($i=0; $i < 6; $i++)
						<div class="form-group">
							<input type="text" name="key_{{$i}}" placeholder="key_{{$i}}" class="form-control">
						</div>
					@endfor
					<div class="form-group">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<button type="submit" class="btn btn-info">Save</button>
					</div>
				</form>
			</div>
		</div>
	</div> -->
</div>

<!-- 	<div class="row" id="users-list">
		<div class="col-md-12">
			<div class="box">
            <div class="box-header">
              <h3 class="box-title">Keywords List</h3>
            </div>
            <div class="box-body">
              <table id="pages-tab" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>#</th>
                  @for($i=0; $i < 6; $i++)
				  <th>KeyLnag {{$i}} </th>
				  @endfor
                  <th><i class="fa fa-trash"></i></th>
                </tr>
                </thead>
                <tbody> -->
<!--               @if(empty($keywords))
 					#No data
              @else
                @foreach ($keywords as $keyword)
	                <tr>
			            @for($i=0; $i < 6; $i++)
						  <td> Abc| <a href=""><i class="fa fa-edit"></i></a></td>
						@endfor
			                  <td><a href="keywords/{$user->id}/bye"><i class="fa fa-trash"></i></a></td>
	                </tr>
				@endforeach	
              @endif -->

<!--                 @for ($row=0; $row < 10; $row++)
	                <tr>
	                <td>#</td>
		            @for($td=0; $td < 6; $td++)
					  <td> Abc| <a href=""><i class="fa fa-edit"></i></a></td>
					@endfor
	                  <td><a href="keywords/{$user->id}/bye"><i class="fa fa-trash"></i></a></td>
	                </tr>
				@endfor
                </tbody>
                <tfoot>
                <tr>
                  <th>#</th>
                  @for($i=0; $i < 6; $i++)
				  <th>KeyLnag {{$i}} </th>
				  @endfor
                  <th><i class="fa fa-trash"></i></th>
                </tr>
                </tfoot>
              </table>
            </div>
        </div>
		</div>
	</div> -->
@endsection
