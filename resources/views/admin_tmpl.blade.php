<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>@yield('head_title', 'Dashboard')</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <link rel="shortcut icon" type="image/x-icon" href="{{asset("favicon.ico")}}">
      
      <link href="{{ asset("/node_modules/admin-lte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
      <!-- Font Awesome -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
      <!-- Ionicons -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
      <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
         page. However, you can choose any other skin. Make sure you
         apply the skin class to the body tag so the changes take effect.
         -->
    <!-- Select2 -->
  <link rel="stylesheet" href="{{asset("/node_modules/admin-lte/plugins/select2/select2.min.css")}}">
      <link rel="stylesheet" href="{{asset("/node_modules/admin-lte/dist/css/skins/skin-blue.min.css")}}">
         <link href="{{asset("/front/css/lightbox.css")}}" rel="stylesheet">
  <link rel="stylesheet" href="{{asset("/node_modules/admin-lte/dist/css/overwrite.css")}}">
   
      <!-- Theme style -->
      <link rel="stylesheet" href="{{ asset("/node_modules/admin-lte/dist/css/AdminLTE.min.css")}}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.1.0/jquery-confirm.min.css">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.1/angular.js"></script>
   @yield('mystyles')
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body class="hold-transition skin-blue sidebar-mini">

<div id="loadingwrap" class="hide">
   <svg xmlns="http://www.w3.org/2000/svg" version="1.1">
     <defs>
       <filter id="gooey">
         <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur"></feGaussianBlur>
         <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7" result="goo"></feColorMatrix>
         <feBlend in="SourceGraphic" in2="goo"></feBlend>
       </filter>
     </defs>
   </svg>
   <div class="blob blob-0"></div>
   <div class="blob blob-1"></div>
   <div class="blob blob-2"></div>
   <div class="blob blob-3"></div>
   <div class="blob blob-4"></div>
   <div class="blob blob-5"></div>
</div>



      <div class="wrapper">
         <!-- Main Header -->
          @include('header')
         <!-- Left side column. contains the logo and sidebar -->
          @include('aside')
         <!-- Content Wrapper. Contains page content -->
         <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
               <h1>
                <!--   {{ $page_title or "Home Page" }} -->
                    @yield('page_title', 'Home')
               </h1>
               <ol class="breadcrumb">
                 <!--  <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                  <li class="active">Here</li> -->
               </ol>
            </section>
            <!-- Main content -->
            <section class="content">
               <!-- Your Page Content Here -->
               <div ng-cloak class="hide"><| public_search |></div> 
                  @yield('content')
            </section>
            <!-- /.content -->
         </div>
         <!-- /.content-wrapper -->
         <!-- Main Footer -->
          @include('footer')
         <!-- Control Sidebar -->
         <!-- Add the sidebar's background. This div must be placed
            immediately after the control sidebar -->
         <div class="control-sidebar-bg"></div>
      </div>
      <!-- ./wrapper -->
      <!-- REQUIRED JS SCRIPTS -->
      <!-- jQuery 2.2.3 -->
      <script src="{{asset("/node_modules/admin-lte/plugins/jQuery/jquery-2.2.3.min.js")}}"></script>
      <!-- Bootstrap 3.3.6 -->
      <script src="{{asset("/node_modules/admin-lte/bootstrap/js/bootstrap.min.js")}}"></script>
      <!-- AdminLTE App -->
      <script src="{{asset("/node_modules/admin-lte/dist/js/app.min.js")}}"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.1.0/jquery-confirm.min.js"></script>

<!-- Select2 -->
<script src="{{asset("/node_modules/admin-lte/plugins/select2/select2.full.min.js")}}"></script>
  <script src="{{asset("/front/js/lightbox.js")}}"></script>
      @yield('myscripts')
      <!-- Optionally, you can add Slimscroll and FastClick plugins.
         Both of these plugins are recommended to enhance the
         user experience. Slimscroll is required when using the
         fixed layout. -->

<script src="{{asset("/node_modules/admin-lte/plugins/slimScroll/jquery.slimscroll.min.js")}}"></script>

   <script>
    $(function() {


        $(".select2").select2();

       
      /*  $(".btn-info").submit(function() {
            $("#loadingwrap").removeClass();
             // $(this).attr('disabled','disabled');
          });*/


        $(".delbtn").click(function(){
         //   e.preventDefault();
         var thisbtn = $(this).parent();
            $.confirm({
              title: 'Confirm!',
              content: 'Simple confirm!',
              buttons: {
                  ok: function () {
                    $("#loadingwrap").removeClass();
                     //$(".delpage").submit();
                     // $(this).closest("form.delpage").submit();

                      $(thisbtn).submit();
                      console.log("done");

                   // $("#loadingwrap").addClass("hide");

                  },
                  cancel: function () {
                     // $.alert('Canceled!');
                  }
              }
            });
        });

  $('#lang-box').slimScroll({
    height: '240px'
  });




   $(".del_a").click(function(e){
            e.preventDefault();
         var this_url = $(this).attr("href");
         var this_parent = $(this).parent();
         var this_parent_class = $(this).parent().attr("class");
            $.confirm({
              title: 'Confirm!',
              content: 'Simple confirm!',
              buttons: {
                  ok: function () {
                    $("#loadingwrap").removeClass();
                     //$(".delpage").submit();
                     // $(this).closest("form.delpage").submit();
                      console.log("done "+this_url);
                  $.get({
                    url: this_url
                  }).done(function(result) {
                     console.log("done "+result);
                     $("#loadingwrap").addClass("hide");
                     $(this_parent).html('<input type="file" name="'+this_parent_class+'">');
                  });

                  },
                  cancel: function () {
                     // $.alert('Canceled!');
                  }
              }
            });
        });


 @yield('snippetscripts')
 
        });
</script> 

        <script type="text/javascript" src="{{ asset("/node_modules/admin-lte/dist/js/main.angular.js")}}"></script>       
   </body>
</html>