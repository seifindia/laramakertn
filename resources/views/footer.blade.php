 <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
       Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; {{ Carbon\Carbon::now()->format('Y') }} <a target="_blank" href="@if($comp != null) {{$comp->url}} @else http://noranus.tn @endif">@if($comp != null) {{$comp->full_name}} @else Noranus Tn @endif</a>.</strong> All rights reserved.
 </footer>