<style type="text/css">
	td {text-align: center;    padding: 10px 2px; }
</style>
<div style="margin: 10px auto; padding: 15px 10px; border: 1px solid #000">
<p style="text-align: right;">Date: <?php echo date('d-M-Y') ?></p>	
<table style="text-align: center; width: 400px; margin: 0 auto;">
	<tr>
		<td><strong>Full Name:</strong></td>
		<td> @if ($request->ctl00ContentPlaceHolder1tbx_name != NULL) {{$request->ctl00ContentPlaceHolder1tbx_name}} @else Not provided @endif</td>
	</tr>
	<tr>
		<td><strong>Contact :</strong></td>
		<td>@if($request->ctl00ContentPlaceHolder1tbx_Phone != NULL) {{$request->ctl00ContentPlaceHolder1tbx_Phone}} @else Not Provided @endif</td>
	</tr>
	<tr>
		<td><strong>Email:</strong></td>
		<td>@if($request->ctl00ContentPlaceHolder1tbx_email != NULL) {{$request->ctl00ContentPlaceHolder1tbx_email}} @else Not Provided @endif</td>
	</tr>
	<tr>
		<td><strong>Address:</strong></td>
		<td>@if($request->ctl00ContentPlaceHolder1tbx_city != NULL){{$request->ctl00ContentPlaceHolder1tbx_city}} @else Not Provided @endif</td>
	</tr>
	@if(!empty($request->ctl00ContentPlaceHolder1chk_aboutyou0))
	<tr>
		<td><strong>I am a :</strong></td>
		<td>@if($request->ctl00ContentPlaceHolder1chk_aboutyou0 != NULL){{$request->ctl00ContentPlaceHolder1chk_aboutyou0}} @else Not Provided @endif</td>
	</tr>
@endif
@if(! empty($request->ctl00ContentPlaceHolder1chk_intrested0))
	<tr>
		<td><strong>I am interested in:</strong></td>
		<td>@if($request->ctl00ContentPlaceHolder1chk_intrested0 != NULL){{$request->ctl00ContentPlaceHolder1chk_intrested0}} @else Not Provided @endif</td>
	</tr>
@endif
@if(! empty($request->ctl00ContentPlaceHolder1tbx_description))
	<tr>
		<td><strong>Brief description of my project:</strong></td>
		<td>@if($request->ctl00ContentPlaceHolder1tbx_description != NULL){{$request->ctl00ContentPlaceHolder1tbx_description}} @else Not Provided @endif</td>
	</tr>
@endif
@if(! empty($request->ctl00ContentPlaceHolder1tbx_Message))

	<tr>
		<td><strong>Message: </strong></td>
		<td>@if($request->ctl00ContentPlaceHolder1tbx_Message != NULL){{$request->ctl00ContentPlaceHolder1tbx_Message}} @else Not Provided @endif</td>
	</tr>
@endif

</table>
</div>
