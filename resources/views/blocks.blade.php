@extends ('admin_tmpl')
	@section('mystyles')


	  <link rel="stylesheet" href="{{asset("/node_modules/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css")}}">
	  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset("/node_modules/admin-lte/plugins/datatables/dataTables.bootstrap.css")}}">
    <!-- Select2 -->
  <link rel="stylesheet" href="{{asset("/node_modules/admin-lte/plugins/select2/select2.min.css")}}">

  	  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style>
 #sortable { list-style-type: none; margin: 0; padding: 5px;  }
 li.ui-state-default {
    margin-bottom: 8px;
}
 /*  #sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 18px; }
  #sortable li span { position: absolute; margin-left: -1.3em; }*/
  </style>

	@endsection

	@section('page_title', 'Blocks')
	@section ('content')
	<div class="row" id="block-form">

		<div class="col-md-12">
				<div id="notif" class="box box-danger box-solid hidden">
		            <div class="box-header with-border">
		              <h3 class="box-title">Alert</h3>

		              <div class="box-tools pull-right">
		                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
		              </div>
		              <!-- /.box-tools -->
		            </div>
		            <!-- /.box-header -->
		            <div class="box-body">
		              Your are in Source mode, your data format will be lost if turn the Source mode off.
		            </div>
		            <!-- /.box-body -->
		          </div>
		</div>

	<form class="form-horizontal" method="POST" action="">
        <div class="col-md-12">
          <div class="box box-info collapsed-box">
            <div class="box-header">
              <h3 class="box-title">Create Block</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body pad">
					<div class="form-group">
			          	<div class="col-md-8">
			          		<input class="form-control" type="text" placeholder="Sub Title" name="sub_title" id="sub_title">
			          	</div>
			          	<div class="col-md-4">
						 <select class="form-control select2" disabled id="select_page" data-placeholder="Select page" required="required"
				            style="width: 100%;" name="page_id">
				            	<option></option>
				                 @foreach ($pages as $cat)
					                <option value="{{$cat->id}}">{{$cat->title}}- {{$cat->code_lang}}</option>
								@endforeach	
				                </select>

			          	</div>



			        </div>
        		<div class="col-md-12">
		            <div class="form-group">
	                   
                    <textarea id="editor1" class="textarea col-md-12" name="sub_content" rows="5"  placeholder="This is my textarea to be replaced with page content.">

						</textarea>                  	
		            </div>	
		          	<div class="form-group">
						<div class="col-md-8">
							<label>Blocks order:</label>
							<ul id="sortable"></ul>
						</div>
						<div class="col-md-4">
							<label>Block style:</label>
								 <select class="form-control select2" id="block_style" data-placeholder="Select Style" required="required"
				            style="width: 100%;" name="block_style">
				                </select>
						</div>
		          	</div>
					<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"/>
                  <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
					<input type="hidden" name="_neworder" id="_neworder"/>
					<div class="form-group">
					 	<button type="button" onclick='saveOrder();' class="btn btn-info">Save</button>
		            </div>
        		</div>
          	</div>
          	<!-- /.boxbody -->
		  </div><!-- box-info -->
		</div>
	</form>
	</div> <!-- #row -->
	<div class="row" id="blocks-list">
		<div class="col-md-12">
			<div class="box">
            <div class="box-header">
              <h3 class="box-title">Blocks List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="pages-tab" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Title</th>
                  <th>Page</th>
                  <th>Order</th>
                  <th>Language</th>
                  <th>Last Updated</th>
                  <th><i class="fa fa-edit"></i></th>
                  <th><i class="fa fa-trash"></i></th>
                </tr>
                </thead>
                <tbody>
              @if(empty($blocks))
 					#No data
              @else
                @foreach ($blocks as $block)
	                <tr>
	                  <td># {{ $block->id }}</td>
	                  <td>{{ $block->sub_title }}</td>
	                  <td><a target="_blank" href="{{action('HomeController@root')}}/{{$block->pagename($block->page_id)}}">{{$block->pagename($block->page_id)}}</a></td>
	                  <td>{{$block->sort_order}}</td>
	                  <td>{{$block->code_lang}}</td>
	                  <td>{{$block->updated_at}}</td>
	                  <td>

	                  <a href="blocks/edit/{{ $block->id }}"><i class="fa fa-edit"></i></a></td>
	                  <td>
	                  <form method="POST" action="blocks/{{ $block->id }}" class="delpage">
	                   {{ csrf_field() }}
	                  <input name="_method" type="hidden" value="DELETE">
	                  	<button type="button" class="delbtn">
	                  		<i class="fa fa-trash"></i>
	                  	</button>
	                  </form>
	             	  </td>
	                </tr>
				@endforeach	
              @endif
                </tbody>
                <tfoot>
                <tr>
                   <th>#</th>
                  <th>Title</th>        
                  <th>Page</th>          
                  <th>Order</th>
                  <th>Last Updated</th>
                  <th>Edit</th>
                  <th>Delete</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
        </div>
          <!-- /.box -->
		</div>
	</div>

		@section ('myscripts')
			<!-- CK Editor -->
<script src="{{asset("/node_modules/admin-lte/plugins/ck/ckeditor.js")}}"></script>
<!-- Bootstrap WYSIHTML5 -->
			<script src="{{asset("/node_modules/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js")}}"></script>
			<!-- DataTables -->
<script src="{{asset("/node_modules/admin-lte/plugins/datatables/jquery.dataTables.min.js")}}"></script>
<script src="{{asset("/node_modules/admin-lte/plugins/datatables/dataTables.bootstrap.min.js")}}"></script>
<!-- SlimScroll -->
<script src="{{asset("/node_modules/admin-lte/plugins/slimScroll/jquery.slimscroll.min.js")}}"></script>
<!-- FastClick -->
<script src="{{asset("/node_modules/admin-lte/plugins/fastclick/fastclick.js")}}"></script>
<!-- Select2 -->
<script src="{{asset("/node_modules/admin-lte/plugins/select2/select2.full.min.js")}}"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		    @endsection

 @section ('snippetscripts')

new_index = 0;
			  	 $( "#sortable" ).sortable();
    			 $( "#sortable" ).disableSelection();
    			/* var data = {!! $pages !!};
			    console.log(data);
			    console.log(data.length);*/


 //$("#sortable").empty();
 			
 			$("#sub_title").focusout(function(){
				if ($("#sub_title").val()!='') {
					$("#select_page").removeAttr('disabled');
					 $('.btn-info').removeAttr('disabled');
					$(this).css('border', '1px solid green');

					    $.get('maxidbloc')
        .success(function(data) {
			 console.log("result data max id"+data);
          new_index = data;
        })
        .error(function(data) { 
            alert('Error: ' + data); 
        }); 

			}
				else {

					$("#select_page").attr('disabled', 'disabled');
					$(this).css('border','1px solid red');
					$("#sortable").empty();
					//$("#sortable li:last-child").remove();
				}

			});


  $("#select_page").change(function(){
  $("#sortable").empty();
    pageID = $(this).val();
    j_sub_title = $("#sub_title").val();
    $.get('blocks_page/'+pageID)
        .success(function(data) {
			 console.log("result length"+data.length);
			 console.log("result data"+data);
			 console.log("result new index"+new_index);

			if (data.length > 0) {
				$(data).each(function(index, value) 
				{
				  	console.log('index'+index);
				    $("#sortable").append("<li class='ui-state-default'  data-article-id='"+data[index].id+"'><span class='ui-icon ui-icon-arrowthick-2-n-s'></span>"+data[index].sub_title+"</li>");
			
				});

			 	$("#sortable").append("<li class='ui-state-default new-sort'  data-article-id='"+new_index+"'><span class='ui-icon ui-icon-arrowthick-2-n-s'></span>"+j_sub_title+"</li>");
			 	$("#_neworder").val(new_index);
			}
			else {

				$("#sortable").append("<li class='ui-state-default'  data-article-id='1'><span class='ui-icon ui-icon-arrowthick-2-n-s'></span>"+j_sub_title+"</li>");
			}

          
        })
        .error(function(data) { 
            //alert('Error: ' + data); 
        }); 


$.get('pages/'+pageID)
        .success(function(data) {
			 console.log("result show page data"+data.page_layout);
			if (data.page_layout == 1) {
			//full page
			$("#block_style").html("<option value='1'>Full</option><option value='2'>Centred</option>");
				            	
			}
			else if(data.page_layout == 2){
			//centred
					$("#block_style").html("<option value='2'>Centred</option>");
			}
			else{
			//right or left sidebar. 3:rightsidebar | 4:leftsidebar
				$("#block_style").html("<option value="+data.page_layout+">Sidebar</option><option value='2'>Centred</option>");
			}

          
        })
        .error(function(data) { 
           alert('Error: ' + data); 
        }); 




  });



			  
			     CKEDITOR.replace('editor1');
   CKEDITOR.config.filebrowserBrowseUrl = 'mediaselect';
			     
				$('#pages-tab').DataTable(); 
				$(".select2").select2();

				var nbr = 0;
				setTimeout(function(){ 
						
				$("#cke_33").click(function(){
					nbr++;
					if (nbr % 2 === 0) { 
					$("#notif").addClass("hidden"); }
   					else { $("#notif").removeClass("hidden");}
				});

					 }, 1000);

		



		@endsection
<script type="text/javascript">
	function saveOrder() {
    var articleorder = "";
    var sub_title, sub_content,page_id, sort_order, code_lang ="";
    
    sub_title 	= $('input[name=sub_title]').val();
    sub_content = CKEDITOR.instances.editor1.getData();
    page_id 	= $('select[name=page_id]').val();
    block_style 	= $('select[name=block_style]').val();
    _token 		= $('input[name=_token]').val();
    sort_order 	= $("#_neworder").val();
    code_lang 	= 'en';
    //code_lang 	= $('select[name=code_lang]').val();

    $("#sortable li").each(function(i) {
        if (articleorder=='')
            articleorder = $(this).attr('data-article-id');
        else
            articleorder += "," + $(this).attr('data-article-id');
    });
            //articleorder now contains a comma separated list of the ID's of the articles in the correct order.
            console.log("bfefore send: order "+sort_order+" -  "+page_id+" - "+code_lang);
    $.post('blocks', { order: articleorder, sub_title:sub_title, sub_content:sub_content, page_id:page_id, block_style:block_style, _token:_token, sort_order: sort_order, code_lang:code_lang })
        .success(function(data) {
            //alert('saved');
              location.reload();
            console.log("result "+data);
        })
        .error(function(data) { 
            //alert('Error: ' + data); 
        }); 
}
</script>
	@endsection