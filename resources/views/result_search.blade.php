@extends ('front.general')
@section ('content')
	@if(count($results) >0 )
		@foreach($results as $result)
			{!! $result->sub_content !!}
			<hr>
		@endforeach
	@else
	<div class="container">
			<div class="row">
		<div class="col-md-12">
				<h3 class="text-center text-danger">Nothing match your request</h3>
		</div>
	</div>
	</div>
	@endif
@endsection