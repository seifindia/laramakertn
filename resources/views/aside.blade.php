<aside class="main-sidebar">
   <!-- sidebar: style can be found in sidebar.less -->
   <section class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
         <div class="pull-left image">
            <img src="{{ asset("node_modules/admin-lte/dist/img/user2-160x160.jpg")}}" class="img-circle" alt="User Image">
         </div>
         <div class="pull-left info">
            <p>{{session()->get('username')}}</p>
            <!-- Status -->
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
         </div>
      </div>
      <!-- search form (Optional) -->
     {{--  <form action="#" method="get" class="sidebar-form">
         <div class="input-group">
            <input type="text" name="q" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
            </button>
            </span>
         </div>
      </form> --}}
      <!-- /.search form -->
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
{{--          <li class="header">HEADER</li>
 --}}         <!-- Optionally, you can add icons to the links -->
         <li class="active"><a target="_blank" href="{{ action('HomeController@root','') }}"><i class="fa fa-dashboard"></i> <span>Home</span></a></li>
         <li><a href="{{ action('PagesController@index','') }}"><i class="fa fa-file-code-o"></i> <span>Pages</span></a></li>
         <li><a href="{{ action('BlocksController@index','') }}"><i class="fa fa-code"></i> <span>Blocks</span></a></li>
         <li><a href="{{ action('MediaController@index','') }}"><i class="fa fa-folder-open-o"></i> <span>Media</span></a></li>
         <li><a href="{{ action('UsersController@index','') }}"><i class="fa fa-users"></i> <span>Users</span></a></li>
         <li><a href="{{ action('LanguagesController@index','') }}"><i class="fa fa-language"></i> <span>Languages</span></a></li>
         <li><a href="{{ action('ConfigController@index','') }}"><i class="fa fa-gears"></i> <span>Configuration</span></a></li>
      </ul>
      <!-- /.sidebar-menu -->
   </section>
   <!-- /.sidebar -->
</aside>