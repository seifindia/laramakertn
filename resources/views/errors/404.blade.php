<!--A Design by W3layouts

Author: W3layout

Author URL: http://w3layouts.com

License: Creative Commons Attribution 3.0 Unported

License URL: http://creativecommons.org/licenses/by/3.0/

-->

<!DOCTYPE HTML>

<html>

<head>

<title>404 error page</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

<link href="{{ asset("front/css/404.css") }}" rel="stylesheet" type="text/css" media="all" />

</head>

<body>

	<!-----start-wrap--------->

	<div class="wrap">

		<!-----start-content--------->

		<div class="content">

			<!-----start-logo--------->

			<div class="logo">

				<h1><a href="#"><img src="{{ asset("front/images/404/logo.png") }}" /></a></h1>

				<span><img src="{{ asset("front/images/404/signal.png") }}" />Oops! The Page you requested was not found!</span>

			</div>

			<!-----end-logo--------->

			<!-----start-search-bar-section--------->

			<div class="buttom">

				<div class="seach_bar">

					<p>you can go to <span><a href="{{action('HomeController@root')}}">home</a></span> page or search here</p>

					<!-----start-sear-box--------->

					

				</div>

			</div>

			<!-----end-sear-bar--------->

		</div>

		<!----copy-right-------------->

	<p class="copy_right">&#169; <?php echo date('Y'); ?> <a href="{{action('HomeController@root')}}" target="_blank">&nbsp; {{config('app.name')}}</a> </p>

	</div>

	

	<!---------end-wrap---------->

</body>

</html>