   <form role="form" method="post" action="config/company" class="" enctype="multipart/form-data">
		              <div class="box-body">
	          				
	          				<div class="row">
	          					<div class="col-md-6">
	          						<div class="form-group">
			          			<input class="form-control" type="text" placeholder="company name" name="full_name" value="{{$comp->full_name}}" required="required">
	        				</div>
							<div class="form-group">
	          					<input class="form-control" type="text" placeholder="short_name" name="short_name" value="{{$comp->short_name}}" required="required">
	        				</div>

							<div class="form-group">
	          					<input class="form-control" type="email" placeholder="email" name="email" value="{{$comp->email}}" required="required">
	        				</div>

							<div class="form-group">
	          					<input class="form-control" type="url" placeholder="url" name="url" value="{{$comp->url}}">
	        				</div>

							<div class="form-group">
			                  <div class="input-group">
			                     <label for="exampleInputFile">Favicon</label>
								<input type="file" name="favicon">
			                  </div>
	        				</div>

							<div class="form-group">
	          					<input class="form-control" type="text" placeholder="adress" name="adress" value="{{$comp->adress}}">
	        				</div>

							<div class="form-group">
	          					<input class="form-control" type="text" placeholder="phone" name="phone" value="{{$comp->phone}}">
	        				</div>

							

	          					</div>
	          					<div class="col-md-6">

	          					<div class="form-group">
	          					<textarea class="form-control" placeholder="description" name="description" rows="2">{{$comp->description}}</textarea>
	        				</div>


	          						<div class="form-group">
								<label>Social networks:</label>
	        				</div>

							<div class="form-group">
	          					<input class="form-control" type="text" placeholder="youtube" name="youtube" value="{{$comp->youtube}}">
	        				</div>
							<div class="form-group">
	          					<input class="form-control" type="text" placeholder="facebook" name="facebook" value="{{$comp->facebook}}" >
	        				</div>
							<div class="form-group">
	          					<input class="form-control" type="text" placeholder="twitter" name="twitter" value="{{$comp->twitter}}">
	        				</div>
							<div class="form-group">
	          					<input class="form-control" type="text" placeholder="linkedin" name="linkedin" value="{{$comp->linkedin}}">
	        				</div>
	          					</div>
	          				</div>
							
		              </div>
		              <!-- /.box-body -->
		              <div class="box-footer">
		                <button type="submit" class="btn btn-info">Update</button>
						<input name="_method" type="hidden" value="PATCH">
				       	<input type="hidden" name="_token" value="{{ csrf_token() }}">
		              </div>
		            </form>