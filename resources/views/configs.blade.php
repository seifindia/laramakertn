
    @extends ('admin_tmpl')
	@section('page_title', 'Configuaration')
	@section ('content')
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary collapsed-box">
		            <div class="box-header with-border">
		              <h3 class="box-title">Company Info</h3> 
		              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
		            </div>
	            	<!-- /.box-header -->
		            <!-- form start -->
		         @if($comp == null)
		         	@include('newcompany')
		         @else
		         	@include ('updatecompany')
		         @endif

		        </div>
			</div>
		</div>


	<div class="row">
		<div class="col-md-8">
				<div class="box box-primary collapsed-box">
		<div class="box-header"><h3 class="box-title">Add/List Menu</h3>

		              <div class="box-tools pull-right">
			            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
			            </button>
			          </div>

			          </div>
		<div class="box-body">
			<div class="row">
				<div class="col-md-6">
				<div class="box box-primary collapse-box">
		            <div class="box-header with-border">
		              <h3 class="box-title">Add Menu</h3> 
		              <div class="box-tools pull-right" style="display: none;">
			            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
			            </button>
			          </div>
		            </div>
	            	<!-- /.box-header -->
		            <!-- form start -->		            
		            <form role="form" method="post" action="config/menu" >
			              <div class="box-body">              
			              <div class="form-group">
			               <div class="row">
			                <div class="col-md-6">
			                  <div class="input-group">
			                        <span class="input-group-addon">
			                          <input type="radio" name="menu_location" id="external-link">
			                        </span>
			                    <input type="text" class="form-control" disabled="disabled" value="External Link">
			                  </div>
			                  <!-- /input-group -->
			                </div>
			                <!-- /.col-md-6 -->
			                <div class="col-md-6">
			                  <div class="input-group">
			                        <span class="input-group-addon">
			                          <input type="radio" name="menu_location" id="inner-link" checked="checked">
			                        </span>
			                    <input type="text" class="form-control" disabled="disabled" value="Inner Link">
			                  </div>
			                  <!-- /input-group -->
			                </div>
                <!-- /.col-md-6 -->
              </div>

			              </div>

			                <div class="form-group">
			                <label for="">Type</label>
			                	<select class="form-control select2" requied="requied" style="width: 100%;"  name="menu_type">
		                		 @foreach ($menutypes as $menutype)
			                		<option id="{{$menutype->id}}">{{$menutype->name}}</option>
			                	  @endforeach
			                	</select>
			                </div>

			                <div class="form-group">
			                <label for="">Lanaguage</label>
								<select class="form-control select2" requied="requied" style="width: 100%;" name="menu_lang">
				                  @foreach ($languages as $lang)
				                    <option value="{{$lang->code_lang}}">{{$lang->name}}</option>
				                  @endforeach
				                </select>	                	
			                </div>
			                <div class="form-group" id="link_pages">
			                <label for="">Link</label>
    						 <select class="form-control select2" id="select_page" data-placeholder="Select page" required="required"
						    style="width: 100%;" name="menu_link_id">
						    	<option></option>
						         @foreach ($pages as $cat)
						            <option value="{{$cat->id}}">{{$cat->title}}</option>
								@endforeach	
						        </select>	         
			                </div>
			                <div class="form-group hide" id="menu_link_external">
			                	<input type="text" name="menu_link_external" class="form-control" placeholder="Link">       	
							</div>

			                <div class="form-group">
			                <label for="">Parent menu</label>
								  <select class="form-control select2" id="select_menu" data-placeholder="Select Parent Menu" required="required"
						    style="width: 100%;" name="menu_parent">
						    	<option value="0">None</option>
						         @foreach ($menus as $menu)
						            <option value="{{$menu->id}}">{{$menu->title}}</option>
								@endforeach	
						        </select>	                  	
			                </div>

			                <div class="form-group">
			                	<input type="text" name="menu_title" class="form-control" placeholder="Title">
			                </div>
			                <div class="form-group">
			                <label for="">Icon</label>
								<input type="text" name="menu_icon" class="form-control">
								</div>
			              

			              </div>
			              <!-- /.box-body -->
			              <div class="box-footer">
			                	<button type="submit" class="btn btn-info">Save</button>
					       		<input type="hidden" name="_token" value="{{ csrf_token() }}">
			              </div>
			        </form>

		        </div>
				</div>
				<div class="col-md-6">
				<div class="box box-primary collapse-box">
		            <div class="box-header with-border">
		              <h3 class="box-title">List Menu</h3> 
		              <div class="box-tools pull-right" style="display: none;">
			            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
			            </button>
			          </div>
		            </div>
	            	<!-- /.box-header -->
		            <!-- form start -->		            
		              <div class="box-body">
						<ul class="list-group">
							  @foreach ($menus as $menu)
										<li class="list-group-item">
									<form method="POST" action="{{action('MenuController@destroy', ['menu' => $menu])}}" 
								 		class="delpage" style="display: inline;float: right;margin-top: 10px;">
				                   		{{ csrf_field() }}
				                  		<input name="_method" type="hidden" value="DELETE">
				                  		<input name="idmenu" type="hidden" value="{{$menu->id}}">
					                  	<button type="button" class="delbtn">
					                  		<i class="fa fa-trash"></i>
					                  	</button>
				                  	</form>
				                  	<img class="img-thumbnail" src="https://upload.wikimedia.org/wikipedia/en/e/e3/CS_Hammam-Lif-Logo-2008.png" width="50" height="50"> 
				                  	<a target="_blank" 
				                  		href="@if($menu->link == 0) 
				                  		{{$menu->external_link}}
				                  		 @else {{action('HomeController@pageById',['id' => $menu->link])}}
				                  		 @endif ">{{$menu->title}}</a> </li>
								@endforeach	
						</ul>
		              </div>
			              <!-- /.box-body -->
			              <div class="box-footer">
			                	<button type="submit" class="btn btn-info">Save</button>
					       		<input type="hidden" name="_token" value="{{ csrf_token() }}">
			              </div>

		        </div>
				</div>

			</div>
		</div>
		<div class="box-footer"></div>
	</div>
		</div>
		<div class="col-md-4">
			<div class="box box-primary collapsed-box">
	            <div class="box-header with-border">
	              <h3 class="box-title">Menu Type</h3> 
	              <div class="box-tools pull-right">
		            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
		            </button>
		          </div>
	            </div>
            	<!-- /.box-header -->
	            <!-- form start -->		            
	            <form role="form" method="post" action="config/menutype" >
		              <div class="box-body">
		              <div class="input-group">
					    <input type="text" class="form-control" name="menu_title" placeholder="Menu Type">
					    <span class="input-group-btn">
					       <button class="btn btn-info" type="submit">Save!</button>
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
					    </span>
					  </div>
		              </div>
		        </form>
		              <!-- /.box-body -->
		              <div class="box-footer">
		                	<ul class="list-group">
		                	 @foreach ($menutypes as $menutype)
								<li class="list-group-item">
								<span>{{$menutype->name}} </span>
									<form method="POST"
								 		class="delpage" style="display: inline-block;float: right;">
				                   		{{ csrf_field() }}
				                  		<input name="_method" type="hidden" value="DELETE">
					                  	<button type="button" class="delbtn">
					                  		<i class="fa fa-trash"></i>
					                  	</button>
				                  	</form>
				                 </li>
			                @endforeach
						</ul>
		              </div>

	        </div>
		</div>
	</div>




		<div class="row">
			<div class="col-md-6">
				<div class="box box-primary collapsed-box">
		            <div class="box-header with-border">
		              <h3 class="box-title">Edit CSS</h3> 
		              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
		            </div>
	            	<!-- /.box-header -->
		            <!-- form start -->
		            <form role="form" method="post" action="config" >
		              <div class="box-body">
		                <label for="">Frontoverwrite.css</label>
							<textarea id="conten_file" class="form-control col-md-12" rows="25" name="content">
								<?php echo file_get_contents('front/css/frontoverwrite.css') ?>
							</textarea>

		              </div>
		              <!-- /.box-body -->
		              <div class="box-footer">
		                	<button type="submit" class="btn btn-info">Save</button>
				       			<input type="hidden" name="file_name" value="front/css/frontoverwrite.css">
				       		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		              </div>
		            </form>
		        </div>
			</div>
			<div class="col-md-6">
				<div class="box box-primary collapsed-box">
		            <div class="box-header with-border">
		              <h3 class="box-title">Edit Js</h3> 
		              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
		            </div>
	            	<!-- /.box-header -->
		            <!-- form start -->
		            <form role="form" method="post" action="config" >
		              <div class="box-body">
		                <label for="">Front overwrite.Js</label>
							<textarea id="conten_file" class="form-control col-md-12" rows="25" name="content">
								<?php echo file_get_contents('front/js/overwrite.js') ?>
							</textarea>

		              </div>
		              <!-- /.box-body -->
		              <div class="box-footer">
		                	<button type="submit" class="btn btn-info">Save</button>
				       			<input type="hidden" name="file_name" value="front/js/overwrite.js">
				       		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		              </div>
		            </form>
		        </div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<p class="text-center">
					<a href="{{action('ConfigController@backupdb')}}">Backup Database</a>
				</p>
			</div>
		</div>

		@section ('snippetscripts')
  			$('#external-link').click(function(){
  				$('#link_pages').hide();
  				$('#link_pages select').removeAttr('required');
  				$('#menu_link_external').removeClass('hide');
  			});	
  			$('#inner-link').click(function(){
  				$('#link_pages').show();
  				$('#link_pages select').attr('required','required');
  				$('#menu_link_external').addClass('hide');
  			});	
		@endsection

	@endsection
