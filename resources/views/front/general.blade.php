<!DOCTYPE html>
<html lang="@yield('page_lang', 'tn')">
<head>
  <meta charset="utf-8">
  <title>@yield('head_title', $comp->full_name)</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <meta name="keywords" content="@section('metas_keys'){{$comp->short_name}} @show"/>
  <meta name="description" content="@section('metas_description')  {{$comp->description}} @show"/>


@if(config('app.name') == 'VadodaraWindows')


    <link rel="stylesheet" href="{{asset("/front/css/owl.theme.css")}}">
    <link href="{{asset("/front/css/base.css")}}" rel="stylesheet">
    <!-- main nav -->
    <link rel="stylesheet" href="{{asset("/front/css/style.css")}}">
    <link href="{{asset("/front/css/bhoechie-tab.css")}}" rel="stylesheet">
    <!-- owl -->
    <link rel="stylesheet" href="{{asset("/front/css/owl.carousel.css")}}">
    <link rel="stylesheet" href="{{asset("/front/css/jquery.fancybox.css?v=2.1.5")}}">

    <link href="{{asset("/front/css/mediaqueries.css")}}" rel="stylesheet">

@elseif(config('app.name') =='Windowmaker Measure')
  <link rel="apple-touch-icon" href="{{asset("front/images/touch/apple-touch-icon.png")}}">
  <link rel="apple-touch-icon" sizes="72x72" href="{{asset("front/images/touch/apple-touch-icon-72x72.png")}}">
  <link rel="apple-touch-icon" sizes="114x114" href="{{asset("front/images/touch/apple-touch-icon-114x114.png")}}">
  <link rel="apple-touch-icon" sizes="144x144" href="{{asset("front/images/touch/apple-touch-icon-144x144.png")}}">
  <meta property="og:title" content="">
  <meta property="og:description" content="">
  <meta property="og:url" content="">
  <meta property="og:image" content="">
  <meta name="format-detection" content="telephone=no">
  <meta name="format-detection" content="address=no">
@else
 {{-- windowmaker --}}
<link href="{{asset("/front/css/flickity.css")}}" rel="stylesheet" >
  <link rel="stylesheet" type="text/css" href="{{asset("/front/css/normal.css")}}">
  <link rel="stylesheet" href="{{asset("/front/css/animation.css")}}">
  <link href="{{asset("/front/css/animate.css")}}" rel="stylesheet">
  <link rel="stylesheet" href="{{asset("/front/css/ticker.css")}}">
  <link href="{{asset("/front/css/font-awesome.min.css")}}" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{asset("/front/css/style.css")}}">
<!--   <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.1.1/ekko-lightbox.min.css"> -->
<link href="{{asset("/front/css/lightbox.css")}}" rel="stylesheet">
  <link rel="stylesheet" href="{{asset("/node_modules/admin-lte/plugins/datatables/dataTables.bootstrap.css")}}">
  <link rel="stylesheet" type="text/css" href="{{asset("/front/css/buttons.css")}}">  
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
  <script>
    if (/mobile/i.test(navigator.userAgent)) document.documentElement.className += ' w-mobile';
  </script>


@endif








  <link href="{{asset("/front/css/bootstrap.min.css")}}" rel="stylesheet">
  <link href="{{asset("/front/css/font-awesome.min.css")}}" rel="stylesheet">
  <link rel="shortcut icon" type="image/x-icon" href="{{asset("favicon.ico")}}">
  <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.min.js"></script><![endif]-->
  @yield('mystyles')  
<link rel="stylesheet" type="text/css" href="{{asset("/front/css/frontoverwrite.css")}}">  
</head>
<body>

  @if(config('app.name') == 'VadodaraWindows')
    @include ('front.includes.nav-menu-vw')
  @elseif(config('app.name') =='Windowmaker Measure')
    @include ('front.includes.nav-menu-wmm')
  @else
    @include ('front.includes.nav-menu-wm')
  @endif
  <div class="main">
    @yield('content')
    {{ csrf_field() }}
  </div>
@if(config('app.name') == 'VadodaraWindows')
    @include ('front.includes.footer-vw')
  @elseif(config('app.name') =='Windowmaker Measure')
    @include ('front.includes.footer-wmm')     
@else
    @include ('front.includes.footer-wm')
@endif
  <script type="text/javascript" src="{{asset("/front/js/jquery.js")}}"></script>
  <script src="{{asset("/front/js/bootstrap.min.js")}}"></script>
  <script src="{{asset("/front/js/modernizr.js")}}"></script>

@if(config('app.name') == 'VadodaraWindows')
  <script src="{{asset("/front/js/main.js")}}"></script>
  <script src="{{asset("/front/js/owl.carousel.js")}}"></script>
  <script src="{{asset("/front/js/filterable.pack.js")}}"></script>
  <script src="{{asset("/front/js/jquery.fancybox.pack.js?v=2.1.5")}}"></script>
  <script src="{{asset("/front/js/jquery.equalheights.min.js")}}"></script>
  <script src="{{asset("/front/js/imagesloaded.pkgd.min.js")}}"></script>
  <script type="text/javascript">
    $(document).ready(function(){
       $('.fancybox').fancybox();



  var sync1 = $("#sync1");
            var sync2 = $("#sync2");

            sync1.owlCarousel({
                singleItem: true,
                slideSpeed: 1000,
                navigation: true,
                pagination: false,
                afterAction: syncPosition,
                responsiveRefreshRate: 200,
                navigationText: ['<img src="../front/images/testimonial-prev.png" alt="prev" class="img-responsive" />', '<img src="../front/images/testimonial-next.png" alt="prev" class="img-responsive" />']
            });

            sync2.owlCarousel({
                items: 4,
                itemsDesktop: [1199, 4],
                itemsDesktopSmall: [979, 4],
                itemsTablet: [768, 4],
                itemsMobile: [479, 4],
                pagination: false,
                responsiveRefreshRate: 100,
                afterInit: function (el) {
                    el.find(".owl-item").eq(0).addClass("synced");
                }
            });

            function syncPosition(el) {
                var current = this.currentItem;
                $("#sync2")
                  .find(".owl-item")
                  .removeClass("synced")
                  .eq(current)
                  .addClass("synced")
                if ($("#sync2").data("owlCarousel") !== undefined) {
                    center(current)
                }
            }

            $("#sync2").on("click", ".owl-item", function (e) {
                e.preventDefault();
                var number = $(this).data("owlItem");
                sync1.trigger("owl.goTo", number);
            });

            function center(number) {
                var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
                var num = number;
                var found = false;
                for (var i in sync2visible) {
                    if (num === sync2visible[i]) {
                        var found = true;
                    }
                }

                if (found === false) {
                    if (num > sync2visible[sync2visible.length - 1]) {
                        sync2.trigger("owl.goTo", num - sync2visible.length + 2)
                    } else {
                        if (num - 1 === -1) {
                            num = 0;
                        }
                        sync2.trigger("owl.goTo", num);
                    }
                } else if (num === sync2visible[sync2visible.length - 1]) {
                    sync2.trigger("owl.goTo", sync2visible[1])
                } else if (num === sync2visible[0]) {
                    sync2.trigger("owl.goTo", num - 1)
                }

            }

//slim scroll with animation
$('.side-nav a').click(function () {
    var target1;
    target1 = $(this).attr("href");
    target1 = $(target1);
    var scrpos = 200;
    if ($('#header').hasClass('affix')) {
        scrpos = 118;
    }
    $("html,body").animate({
        scrollTop: ((target1.offset().top) - scrpos)
    }, 1000);
    return false;
});



$('portfolio-list').filterable({
useHash: true,
animationSpeed: 1000,
show: { width: 'show', opacity: 'show' },
hide: { width: 'hide', opacity: 'hide' },
useTags: true,
tagSelector: '#portfolio-filter a',
selectedTagClass: 'current',
allTag: 'all'
});

    });
  </script>
  @elseif(config('app.name') =='Windowmaker Measure')

  <script src="{{asset("/front/js/library.js")}}"></script>
  <script src="{{asset("/front/js/script.js")}}"></script>
  <script src="{{asset("/front/js/retina.js")}}"></script>
  

@else
  <script type="text/javascript" src="{{asset("/front/js/jquery.js")}}"></script>
  <script src="{{asset("/front/js/bootstrap.min.js")}}"></script>
  <script src="{{asset("/front/js/flickity.pkgd.min.js")}}"></script>
  <script type="text/javascript" src="{{asset("/front/js/normal.js")}}"></script>
  <script type="text/javascript" src="{{asset("/front/js/carousels.js")}}"></script>
  <script type="text/javascript" src="{{asset("/front/js/slider-modernizr.js")}}"></script>
  <script src="{{asset("/front/js/classie.js")}}"></script>
  <script src="{{asset("/front/js/toucheffects.js")}}"></script>
  <script src="{{asset("/front/js/animation.js")}}"></script>
  <script src="{{asset("/front/js/ticker.js")}}"></script>
  <script src="{{asset("/front/js/waypoints.min.js")}}"></script>
  <script src="//cdn.jsdelivr.net/jquery.scrollto/2.1.2/jquery.scrollTo.min.js"></script>
  <script src="{{asset("/front/js/lightbox.js")}}"></script>
        <!-- DataTables -->
<script src="{{asset("/node_modules/admin-lte/plugins/datatables/jquery.dataTables.min.js")}}"></script>
<script src="{{asset("/node_modules/admin-lte/plugins/datatables/dataTables.bootstrap.min.js")}}"></script>
  <script>
$(document).ready(function() {  
var scrolled=0;

      $("#go2top").on("click" ,function(){
          scrolled=scrolled+30;
      $("body").animate({
          scrollTop:  scrolled
           });
      });


$('.wp3').waypoint(function() {
    $('.wp3').addClass('animated bounceInDown');
  }, {
    offset: '75%'
  });

  $('#showcaseSlider').flickity({
    cellAlign: 'left',
    
    contain: true,
    prevNextButtons: false,
    imagesLoaded: true
  });



 
    });

  </script>


@endif
  @yield('myjs_scripts')
  <script>
    $(document).ready(function() {  

          $(".contact_form").append($("input[name=_token]"));
          $('#lightbox-grp').append("<div class='clearfix'></div>");
          $('#lightbox-grp').css("padding", "20px 0px");
          $('#lightbox-grp a').attr('data-lightbox','roadmap');
          $('#lightbox-grp a').addClass('col-md-3');
          $('#lightbox-grp a img').addClass('img-thumbnail');
          $('#lightbox-grp a img').removeAttr('style');



     @yield('myscripts')
    });
  </script>
  <script src="{{asset("/front/js/overwrite.js")}}"></script>
</body>
</html>