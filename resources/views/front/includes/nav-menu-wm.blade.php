  <div class="fix-header">
    <div class="w-container">
      <div class="w-nav" data-collapse="medium" data-animation="default" data-duration="400"></div>
    </div>
  </div>
  <div class="fixed-header">
      <div class="container">
      <div class="w-row">
        <div class="w-col w-col-3 logo">
          <a href="{{action('HomeController@root')}}#home"><img class="logo" src="{{asset("/front/images/logo.png")}}" alt="Windowmaker" title="The world’s leading software for the window and door industry."></a>
        </div>
        <div class="w-col w-col-9">
          <div class="w-nav navbar" data-collapse="medium" data-animation="default" data-duration="400" data-contain="1">
            <div class="w-container nav">
              <nav class="w-nav-menu nav-menu" role="navigation">
                <a class="w-nav-link menu-li" href="{{action('HomeController@root')}}#home">HOME</a>
                <a class="w-nav-link menu-li" href="{{action('HomeController@internal', ['lang'=>'en','page'=>'company'])}}">Company</a>
                <a class="w-nav-link menu-li" href="{{action('HomeController@root')}}#solutions">SOLUTIONS</a>
                <a class="w-nav-link menu-li" href="{{action('HomeController@root')}}#services">SERVICES</a>
                <a class="w-nav-link menu-li" href="{{action('HomeController@internal', ['lang'=>'en','page'=>'carrers'])}}">Careers</a>
                <a class="w-nav-link menu-li" href="{{action('HomeController@internal', ['lang'=>'en','page'=>'contact'])}}">CONTACT
              </nav>
              <div class="w-nav-button">
                <div class="w-icon-nav-menu"></div>
              </div>

              <ul>
              @foreach ($langs as $ourlang)
                <li><a href="{{action('HomeController@pageByLang', ['lang'=>$ourlang->code_lang,'equiv'=>$equivalance->id])}}">{{$ourlang->name}}</a></li>
              @endforeach  
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>



