  <div class="footer-parlex">
    <div class="parlex9-back">
      <div class="w-container">
        <div class="wrap">
          <img class="footer-logo" src="{{asset("/front/images/footer-logo.png")}}" alt=" {{$comp->full_name}}">
          <div class="footer-social">
            <div class="fotter-social-wrap">
              <a href="https://www.facebook.com/" target="_blank"><img class="fotter-social" src="{{asset("/front/images/social/Facebook.png")}}" alt="52dd249c929b601f5400054c_Facebook.png"></a>
              <a href="https://www.pinterest.com/" target="_blank"><img class="fotter-social" src="{{asset("/front/images/social/Pinterest.png")}}" alt="52de533c5d3566c1430003e9_Pinterest.png"></a>
              <a href="https://www.twitter.com/" target="_blank"><img class="fotter-social" src="{{asset("/front/images/social/Twitter.png")}}" alt="52dd24f2929b601f54000551_Twitter.png"></a>
              <a href="https://www.youtube.com/" target="_blank"><img class="fotter-social" src="{{asset("/front/images/social/Youtube.png")}}" alt="52de54495d3566c14300040a_Youtube.png"></a>
            </div>
          </div>
          <div>
            <div class="fotter-text"><p class="fotter-quote">“ The world’s leading software for the window and door industry. ”</p>
              <p class="copyright-area">©<?php echo date('Y'); ?> {{$comp->full_name}}. ALL RIGHTS RESERVED</p></div>
          </div>
        </div>
      </div>
    </div>
  </div>