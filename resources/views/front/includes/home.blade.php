@extends ('front.general')

@section('page_lang'){{$c_page->code_lang}}@endsection
@section('metas_keys')
  @if($c_page->keywords != NULL) {{$c_page->keywords}} @else @parent @endif
@endsection
@section('metas_description')
  @if($c_page->description != NULL) {{$c_page->description}} @else @parent @endif
@endsection
@section('head_title') @if($c_page->title == 'index') {{$comp->full_name}} @else {{$c_page->title}} @endif @endsection
@section('mystyles')
@if ($c_page->css_url != NULL)
    <link rel="stylesheet" href="{{asset("/front/$c_page->css_url")}}">
@endif

@endsection
@section ('content')
<div class="@if($c_page->page_layout == 1) container-fluid 
            @elseif($c_page->page_layout == 4)
            @elseif($c_page->page_layout == 3)
            @else container 
            @endif 
            container-{{$c_page->id}} c-{{$c_page->code_lang}}" 
 id="page_{{$equivalance->id}}">
{{-- $equivalance->id --}}
{!! $c_page->content !!}


  @foreach  ($blocks->sortBy('sort_order') as $blck)
  <div class="blk_{{$blck->id}} @if($blck->block_layout == 1) container-fluid @else container @endif ">
    {!! $blck->sub_content !!}
  </div>
  @endforeach
</div>



@endsection

@section ('myjs_scripts')
  @if ($c_page->js_url != NULL)
  <script type="text/javascript"  src="{{asset("/front/$c_page->js_url")}}"></script> {{-- without front folder --}}
  @endif
@endsection

@section ('myscripts')
  {!! $c_page->js_code !!}
@endsection