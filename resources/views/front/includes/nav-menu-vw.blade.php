<div data-spy="affix" data-offset-top="50" id="header" class="affix-top">
    <div class="blue-line hidden-xs" id="top"></div>
    <div class="cd-main-header">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 col-sm-offset-0 col-xs-6 col-xs-offset-3 pad-xs-reset">
                    <a href="{{action('HomeController@root')}}">
                        <img src="{{asset("/front/images/vadodara-windows.png")}}" alt="Vadodara Windows" class="img-responsive logo"></a>
                </div>
                <!-- nav begins -->
                <div class="col-sm-9 col-xs-12">
                    <div class="header-btns">
                        <div class="btn btn-default btn-block" style="cursor:default;">+91 97249 61541</div>
                        <a class="btn btn-default btn-block" href="request-quote">REQUEST QUOTE</a>
                    </div>
                    <!-- buttons for mobile begin -->
                    <nav class="cd-nav">
    <ul id="cd-primary-nav" class="cd-primary-nav is-fixed">
        <li>
              <a href="{{url('en/company')}}">COMPANY</a></li>

        <li class="has-children">
              <a href="{{url('en/windows')}}">WINDOWS</a>

              <ul class="cd-nav-gallery is-hidden" style="left: -130px;width: 60%;">
                  <li class="go-back"><a href="#0">Back</a></li>
                  <li class="see-all visible-xs visible-sm"><a href="{{url('en/windows')}}">All Windows</a></li>
                  
                          <li class="visible-xs visible-sm"><a href="{{url('en/Casement-Windows')}}">Casement Windows</a></li>
                      
                          <li class="visible-xs visible-sm"><a href="{{url('en/Sliding-Windows')}}">Sliding Windows</a></li>
                      
                  <li class="visible-xs visible-sm"><p class="EmptyMessage"></p></li>
                  <li class="hidden-xs hidden-sm">
                      <div class="col-sm-12">
                          <div class="row">
                              
                                      <div class="col-sm-6 nav-img-box">
                                          <p><a href="{{url('en/Casement-Windows')}}">Casement Windows</a></p>
                                          <a href="{{url('en/Casement-Windows')}}"><img src="{{asset("/front/images/20372015103742Casement-Window-1.png")}}" class="img-responsive" alt="Casement Windows"></a>
                                      </div>
                                  
                                      <div class="col-sm- 6 nav-img-box">
                                          <p><a href="{{url('en/Sliding-Windows')}}">Sliding Windows</a></p>
                                          <a href="{{url('en/Sliding-Windows')}}"><img src="{{asset("/front/images/20342015103446Sliding-Window.png")}}" class="img-responsive" alt="Sliding Windows"></a>
                                      </div>
                          <p class="EmptyMessage"></p>
                          
                          
                      </div>
                  </li>
              </ul>

          </li>

        <li class="has-children">
              <a href="{{url('en/doors')}}" class="">DOORS</a>
              <ul class="cd-nav-gallery is-hidden">
                  <li class="go-back"><a href="http://www.vadodarawindows.com/#0">Back</a></li>
                  <li class="see-all visible-xs visible-sm"><a href="{{url('en/doors')}}">All Doors</a></li>
                  
                          <li class="visible-xs visible-sm"><a href="{{url('en/Casement-doors')}}">Casement Doors</a></li>
                      
                          <li class="visible-xs visible-sm"><a href="{{url('en/Sliding-Doors')}}">Sliding Doors</a></li>
                      
                          <li class="visible-xs visible-sm"><a href="{{url('en/French-Doors')}}">French Doors</a></li>
                      
                          <li class="visible-xs visible-sm"><a href="{{url('en/Patio')}}">Patio &amp; Bi-Fold Doors</a></li>
                      
                  <li class="visible-xs visible-sm"><p class="EmptyMessage"></p></li>
                  <li class="hidden-xs hidden-sm">
                      <div class="col-sm-12">
                          <div class="row">
                              
                                      <div class="col-sm-3 nav-img-box">
                                          <p><a href="{{url('en/Casement-doors')}}">Casement Doors</a></p>
                                          <a href="{{url('en/Casement-doors')}}"><img src="{{asset("/front/images/20422015104201Casement-Door1.png")}}" class="img-responsive" alt="Casement Doors"></a>
                                      </div>
                                  
                                      <div class="col-sm-3 nav-img-box">
                                          <p><a href="{{url('en/Sliding-Doors')}}">Sliding Doors</a></p>
                                          <a href="{{url('en/Sliding-Doors')}}"><img src="{{asset("/front/images/20402015104040Sliding-Door1.png")}}" class="img-responsive" alt="Sliding Doors"></a>
                                      </div>
                                  
                                      <div class="col-sm-3 nav-img-box">
                                          <p><a href="{{url('en/French-Doors')}}">French Doors</a></p>
                                          <a href="{{url('en/French-Doors')}}"><img src="{{asset("/front/images/20422015104238French-Door1.png")}}" class="img-responsive" alt="French Doors"></a>
                                      </div>
                                  
                                      <div class="col-sm-3 nav-img-box">
                                          <p><a href="{{url('en/Patio')}}">Patio &amp; Bi-Fold Doors</a></p>
                                          <a href="{{url('en/Patio')}}"><img src="{{asset("/front/images/20432015104321Bi-Fold-Door1.png")}}" class="img-responsive" alt="Patio &amp; Bi-Fold Doors"></a>
                                      </div>
                                  
                          </div>
                          <p class="EmptyMessage"></p>
                          
                      </div>
                  </li>
              </ul>
          </li>

        <li>
              <a href="{{url('en/portfolio')}}">PORTFOLIO</a></li>
        <li><a href="{{url('en/testimonials')}}">Testimonials</a></li>
        <li>
              <a href="{{url('en/contact')}}">CONTACT US</a></li>
        <li>
		<span class="searchborder">
    <form action="{{action('HomeController@search_for')}}" method="get">
            <input name="Keywords" type="text" id="ctl00_nav_tbx_search_web" required="required" class="fonm-control search" placeholder="Search Keyword">
			<button style="border: 0px;" type="submit" id="ctl00_nav_btn_searchbtn" class="searchbtn">
            <span id="ctl00_nav_RequiredFieldValidator1" style="color:Red;display:none;"></span></button>
</form>
			</span>
        </li>
    </ul>
    <!-- primary-nav -->
</nav><ul class="cd-header-buttons visible-xs visible-sm">
                        <!--<li><a class="cd-search-trigger" href="#cd-search">Search<span></span></a></li>-->
                        <li><a class="cd-nav-trigger" href="http://www.vadodarawindows.com/#cd-primary-nav"><span></span></a></li>
                        <li>Click to Navigate</li>
                        <li class="clearfix"></li>
                    </ul>
                    <!-- buttons for mobile end -->

                </div>
                <!-- nav ends -->
            </div>
        </div>
    </div>
</div>