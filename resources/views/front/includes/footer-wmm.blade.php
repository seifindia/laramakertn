<footer>
<div class="wrap">
    <p>&copy; {{ Carbon\Carbon::now()->format('Y') }} <strong> <a target="_blank" href="@if($comp != null) {{$comp->url}} @else http://noranus.tn @endif">@if($comp != null) {{$comp->full_name}} @else Noranus Tn @endif</a></strong>, All Rights Reserved</p>
</div>
<!-- /.wrap -->
</footer>