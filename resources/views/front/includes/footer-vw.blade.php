<div class="footer1">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-sm-4 hidden-xs">
                <div class="row">
                    <div class="col-sm-12">
                        <h3 class="footer-heading">Quick Links</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <ul class="text-content">
                            <li><a href="../">Home</a></li>
                            <li><a href="{{url('en/company')}}">Company</a></li>
                            <li><a href="{{url('en/windows')}}">Windows</a></li>
                            <li><a href="{{url('en/doors')}}">Doors</a></li>
                            <li><a href="{{url('en/portfolio')}}">Portfolio</a></li>
                            <li><a href="{{url('en/testimonials')}}">Testimonials</a></li>
                            <li><a href="{{url('en/contact')}}">Contact Us</a></li>
                        </ul>
                        <div class="social-links">
                            
                            <a href="https://www.linkedin.com/company/vadodara-windows-pvt--ltd-" target="_blank">
                                <img src="{{asset("/front/images/linkdin-icon.png")}}" alt="linkdin" class="img-responsive"></a>
                            <a href="https://www.instagram.com/vadodarawindows/" target="_blank">
                                <img src="{{asset("/front/images/insta-icon.png")}}" alt="instagram" class="img-responsive"></a>
                            <a href="https://plus.google.com/111035697402817328791/about" target="_blank">
                                <img src="{{asset("/front/images/gplus-icon.png")}}" alt="gplus" class="img-responsive"></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 hidden-xs">
                <div class="address-block">
                    <div class="row">
                        <div class="col-sm-12">
                            <h3 class="footer-heading">Office Address</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2 pad-right-reset">
                            <a href="{{url('en/contact')}}">
                                <img src="{{asset("/front/images/location-icon.png")}}" class="img-responsive" alt=""></a>
                        </div>
                        <div class="col-sm-10">
                            <p class="text-content">
                                Vadodara Windows Pvt. Ltd.<br>
                                3rd Floor, Kalapi Avenue,<br>
                                20 Punit Nagar, Old Padra Road,<br>
                                Vadodara - 390015, Gujarat, INDIA
                            </p>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-2 pad-right-reset">
                            <a href="{{url('en/contact')}}">
                                <img src="{{asset("/front/images/phone-mail-icon-grey.png")}}" class="img-responsive" alt=""></a>
                        </div>
                        <div class="col-sm-10">
                            <p class="text-content">
                                +91 97249 61541<br>
                                <a href="mailto:info@vadodarawindows.com">info@vadodarawindows.com</a>
                            </p>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 hidden-xs">
                <div class="footer-form">
                    <div class="row">
                        <div class="col-sm-12">
                            <h3 class="footer-heading">Enquire Now!</h3>
                            <div class="form-horizontal">
                            <form action="contact/send" method="post" class="contact_form">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <span id="ctl00_footer_rfvname" style="color:Red;display:none;"></span>
                                        <input name="ctl00ContentPlaceHolder1tbx_name" type="text" maxlength="255" id="ctl00_footer_tbx_name" class="form-control" placeholder="Name" required="required">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <span id="ctl00_footer_RequiredFieldValidator3" style="color:Red;display:none;"></span>
                                        <span id="ctl00_footer_RegularExpressionValidator2" style="color:Red;display:none;"></span>
                                        <input name="ctl00ContentPlaceHolder1tbx_email" type="email" maxlength="255" id="ctl00_footer_tbx_email" class="form-control" placeholder="Email" required="required">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <span id="ctl00_footer_RequiredFieldValidator1" style="color:Red;display:none;"></span>
                                        
                                        <input name="ctl00ContentPlaceHolder1tbx_Phone" type="number" maxlength="15" id="ctl00_footer_tbx_Phone" class="form-control" placeholder="Contact No.">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <textarea name="ctl00ContentPlaceHolder1tbx_Message" rows="2" cols="20" required="required" id="ctl00_footer_tbx_Message" class="form-control" placeholder="Message"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <input type="submit" name="ctl00footersubmit" value="Submit" id="ctl00_footer_submit" class="btn btn-default submit">

                                        <input type="hidden" name="footer_form" value="1" />
                                        <div id="ctl00_footer_validatesummary" style="color:Red;display:none;">

                                </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-12">
                <div class="sitemap-block">
                    <p class="text-content small">
                        <span class="left-sm"><a href="{{url('en/sitemap')}}" class="large">Sitemap</a><span class="visible-xs-inline visible-sm-inline">&nbsp;|&nbsp;</span><br class="hidden-xs hidden-sm">
                            © {{ Carbon\Carbon::now()->format('Y') }} Vadodara Windows Pvt. Ltd.</span><br class="hidden-sm">
                        <br class="hidden-xs hidden-sm">
                        <span class="right-sm"></span>
                    </p>
                </div>
                <div class="social-links-m visible-xs">
                    
                    <a href="https://www.linkedin.com/profile/view?id=AAkAABvyAaYBzvLBJS3gQvQ7S11SbFm4xsZW-9U&amp;authType=NAME_SEARCH&amp;authToken=gZLF&amp;locale=en_US&amp;trk=tyah&amp;trkInfo=clickedVertical%3Amynetwork%2CclickedEntityId%3A468844966%2CauthType%3ANAME_SEARCH%2Cidx%3A1-1-1%2CtarId%3A1451026467912%2Ctas%3Avadodara" target="_blank">
                        <img src="{{asset("/front/images/linkdin-icon.png")}}" alt="linkdin" class="img-responsive"></a>
                    <a href="https://www.instagram.com/vadodarawindows/" target="_blank">
                        <img src="{{asset("/front/images/insta-icon.png")}}" alt="instagram" class="img-responsive"></a>
                    <a href="https://plus.google.com/111035697402817328791/about" target="_blank">
                        <img src="{{asset("/front/images/gplus-icon.png")}}" alt="gplus" class="img-responsive"></a>
                </div>
            </div>
        </div>
    </div>
</div>