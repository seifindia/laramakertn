@extends ('front.single')

@section('metas')  
 <meta name="keywords" content="{{$c_page->keywords}}" />
 <meta name="description" content="{{$c_page->description}}" />
@endsection


@section('head_title')
  {{ $c_page->title }}
@endsection

@section('mystyles')
@if ($c_page->css_url != NULL)
    <link rel="stylesheet" href="{{asset("/front/$c_page->css_url")}}">
@endif

<style type="text/css">

  .slogan-text h1 {color: #000;}
  .portfolio-text p, p, h3{color: #000 !important ;}
  p a {color: red; text-decoration: underline;}
  img.example-image {
    width: 20%;
    margin: 1%;
}
#page_{{ $c_page->id }} .plan1{
    border-width: 1px 1px 4px !important;
}
</style>
@endsection


@section ('content')

    <div class="portfolio-parlex" id="page_{{ $c_page->id }}">
    <div class="mission-parlex3-back">
      <div class="" style="width: 100%; overflow: hidden;"> <!-- w-container -->
        <div class="wrap">
          <div class="portfolio">
            <h1 class="portfolio-heading">{{ $c_page->title }}</h1>
            <div class="portfolio-text">
              {!! $c_page->content !!}
              <br>
                 @if ($c_page->img_name != NULL)
                 <img src="{{asset("/front/$c_page->img_name")}}">
                 @endif
            </div>
          </div>
            <div class="sepreater"></div>
            <div class="w-container">
               <div class="row">
              <div class="col-md-12">
                @foreach  ($blocks->sortBy('sort_order') as $blck)
                  {!! $blck->sub_content !!}
                @endforeach


              </div>
            </div>
            </div>
           <!-- /container -->
        </div>
      </div>
    </div>
  </div>
  @endsection

    @section ('myjs_scripts')
      @if ($c_page->js_url != NULL)
      <script type="text/javascript"  src="{{asset("/front/$c_page->js_url")}}"></script>
      @endif
    @endsection

    @section ('myscripts')
      {!! $c_page->js_code !!}
    @endsection