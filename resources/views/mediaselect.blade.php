<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Example: Browsing Files</title> 
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</head>
<body>
        <div class="container" id="media-from">
            <div class="col-md-12">
                <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Upload Files</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body pad">
                    <div class="col-md-12">
                        <form class="form-horizontal" method="POST" action="media" enctype="multipart/form-data">
                            <div class="form-group">
                                <input class="form-control" type="file" name="mediafile" multiple="multiple">
                            </div>
                            <div class="form-group">
                                <select class="form-control select2" name="folder">
                                    <option value="images">Images</option>
                                    <option value="pdf">PDF</option>
                                    <option value="assets">Others</option>
                                </select>
                                                        </div>
                            <input type="hidden" name="file_toshow">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                            <div class="form-group">
                                <button type="submit" class="btn btn-info">Upload</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.boxbody -->
            </div><!-- box-info -->
            </div>
        </div>



        <div class="fluid-container">
    <h3>{{count($imagesfiles)}} Images files</h3>
        @foreach ($imagesfiles as $imgfile)
            <div class="col-md-2" style="float: left;margin: 5px; display: inline-block; width: 24%;position: relative;">
                    <img style="width: 90%;height: auto;" src="{{asset($imgfile->getPathname())}}" class="img-responsive img-thumbnail">

                    <button  class="fileUrl" id="{{asset($imgfile->getPathname())}}" style="position: absolute;z-index: 999; top: 10px;right: 40px;">Select File</button>
            </div>
        @endforeach
            
        </div>
   
    <script>
        $(".fileUrl").click(function(){
          var funcNum = getUrlParam( 'CKEditorFuncNum' );
          var fileUrl = $(this).attr('id');
            window.opener.CKEDITOR.tools.callFunction( funcNum, fileUrl );
            window.close();
        });

         
        //http://docs.ckeditor.com/#!/guide/dev_file_browser_api
        // Helper function to get parameters from the query string.
        function getUrlParam( paramName ) {
            var reParam = new RegExp( '(?:[\?&]|&)' + paramName + '=([^&]+)', 'i' );
            var match = window.location.search.match( reParam );

            return ( match && match.length > 1 ) ? match[1] : null;
        }

    </script>

</body>
</html>