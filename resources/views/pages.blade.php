@extends ('admin_tmpl')
	@section('mystyles')
	  <link rel="stylesheet" href="{{asset("/node_modules/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css")}}">
	  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset("/node_modules/admin-lte/plugins/datatables/dataTables.bootstrap.css")}}">

	@endsection

	@section('page_title', 'Pages')
	@section ('content')
  
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


	<div class="row" id="page-form">
		<form class="form-horizontal" method="POST" enctype="multipart/form-data" action="{{action('PagesController@store','')}}">
        <div class="col-md-12">
          <div class="box box-info collapsed-box">
            <div class="box-header">
              <h3 class="box-title">Page Content</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body pad">
        		<div class="col-md-6">
			<div class="form-group">
	          	<input class="form-control" type="text" placeholder="Title" name="title" required="required">
	        </div>
            <div class="form-group">
                <textarea id="editor1" class="textarea col-md-12" name="content" rows="5"  placeholder="This is textarea to be replaced with page content.">
				</textarea>                  	
            </div>

	        <div class="form-group hidden">
	              <div class="input-group">
	                <span class="input-group-addon"><i class="fa fa-link"></i></span>
	                <input type="text" class="form-control" placeholder="Url will be generated automatically but you can change it">
	              </div>
	        </div>
            </div>


            		<div class="col-md-6">
                      <div class="form-group">
                <label>Language</label>
                <select class="form-control select2" requied="requied" style="width: 50%;" name="code_lang">
                  @foreach ($languages as $lang)
                    <option value="{{$lang->code_lang}}">{{$lang->name}}</option>
                  @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Layout</label>
                <select class="form-control select2" requied="requied" style="width: 50%;" name="page_layout">
                    <option value="1">Full width</option>
                    <option value="2">Centred</option>
                    <option value="3">Right Sidebar</option>
                    <option value="4">Left Sidebar</option>
                </select>
            </div>
                  <div class="form-group">
                    <input class="form-control" type="text" placeholder="Keywords" name="keywords">
                  </div>
                  <div class="form-group">
                    <input class="form-control" type="text" placeholder="description" name="description">
                  </div>

            <div class="form-group">
              <label for="exampleInputFile">Image</label>
              <input type="file" id="page-image" name="img_name">
            </div>

	            		<div class="form-group">
                  <div class="input-group">
                     <label for="exampleInputFile">Css File(s)</label>
                      <input type="file" id="page-css" name="css_url"> 
                  </div>
	            		   
	            		</div>
	            		<div class="form-group">
                  <div class="input-group">
                      <label for="exampleInputFile">Js File(s)</label>
                      <input type="file" id="page-js" name="js_url"> 
                  </div>
	            		</div>
	            		<div class="form-group">
	              			 <textarea class="form-control" rows="2" placeholder="Enter your javascript code ..." name="js_code"></textarea>
	            		</div>
      <div class="form-group">
                <label>
                  <input type="radio" value="1" name="state" class="flat-red" >      Published
                </label>
                <label>
                  <input type="radio" value="2" name="state" class="flat-red">      Review
                </label>
                <label>
                  <input type="radio" value="0" name="state" class="flat-red" checked>
                  Draft
                </label>
            </div>

	            		<input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
	            		<div class="form-group">
	              			 <button type="submit" class="btn btn-info">Save</button>
	            		</div>

		</div> <!-- #2nd col-6 -->




        		</div>
          	</div>
          <!-- /.box -->
		</div>

		</form>
	</div> <!-- #row -->
	<div class="row" id="page-list">
		<div class="col-md-12">
			<div class="box">
            <div class="box-header">
              <h3 class="box-title">Pages List </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="pages-tab" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Title</th>
                  <th>Language</th>
                  <th>Url page</th>
                  <th>Last Updated</th>
                  <th><i class="fa fa-edit"></i></th>
                  <th><i class="fa fa-trash"></i></th>
                </tr>
                </thead>
                <tbody>
                  @if(empty($pages))
                    #No data
                  @else
                      @foreach ($pages->sortBy('id') as $page)
                        <tr>
                          <td># {{ $page->id }}</td>
                          <td>{{ $page->title }}</td>
                          <td>{{$page->code_lang}}</td>
                          <td><a target="_blank" href="{{action('HomeController@root')}}/{{$page->page_url}}">{{$page->page_url}}</a></td>
                          <td>{{$page->updated_at}}</td>
                          <td>
                          <a href="{{action('PagesController@edit', ['page' => $page->id])}}"><i class="fa fa-edit"></i></a></td>
                          <td>
                            <form method="POST" action="{{action('PagesController@remove', ['page' => $page->id])}}" class="delpage">
                             {{ csrf_field() }}
                            <input name="_method" type="hidden" value="DELETE">
                              <button type="button" class="delbtn">
                                <i class="fa fa-trash"></i>
                              </button>
                            </form>
                          </td>
                        </tr>
                      @endforeach 
                  @endif
                </tbody>
                <tfoot>
                <tr>
                   <th>#</th>
                  <th>Title</th>
                  <th>Language</th>
                  <th>Url page</th>
                  <th>Last Updated</th>
                  <th>Edit</th>
                  <th>Delete</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
        </div>
          <!-- /.box -->
		</div>
	</div>

		@section ('myscripts')
			<!-- CK Editor -->
			<script src="{{asset("/node_modules/admin-lte/plugins/ck/ckeditor.js")}}"></script>
			<!-- Bootstrap WYSIHTML5 -->
			<script src="{{asset("/node_modules/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js")}}"></script>
			<!-- DataTables -->
<script src="{{asset("/node_modules/admin-lte/plugins/datatables/jquery.dataTables.min.js")}}"></script>
<script src="{{asset("/node_modules/admin-lte/plugins/datatables/dataTables.bootstrap.min.js")}}"></script>
<!-- SlimScroll -->
<script src="{{asset("/node_modules/admin-lte/plugins/slimScroll/jquery.slimscroll.min.js")}}"></script>
<!-- FastClick -->
<script src="{{asset("/node_modules/admin-lte/plugins/fastclick/fastclick.js")}}"></script>
  @endsection

 @section ('snippetscripts')

			   
			    CKEDITOR.replace('editor1');
   CKEDITOR.config.filebrowserBrowseUrl = 'mediaselect';

          $('#pages-tab').DataTable(); 


			    //bootstrap WYSIHTML5 - text editor
			   // $(".textarea").wysihtml5();
        $('.btn-info').attr('disabled','disabled');


          $(".btn-info").submit(function() {
            $("#loadingwrap").removeClass();
             // $(this).attr('disabled','disabled');
          });
			
    $("input[name=title]").focusout(function(){
        if ($(this).val()!='') {
        $('.btn-info').removeAttr('disabled');
        }

        });
	    @endsection


	@endsection