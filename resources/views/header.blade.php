<header class="main-header">
   <!-- Logo -->
   <a href="{{action('HomeController@root')}}" class="logo" target="_blank">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>@if($comp != null) {{$comp->short_name}} @else TN @endif</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>@if($comp != null) {{$comp->full_name}} @else Noranus Tn @endif</b></span>
   </a>
   <!-- Header Navbar -->
   <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
         <ul class="nav navbar-nav">
            <!-- Notifications Menu -->
            <!-- Tasks Menu -->
            <!-- User Account Menu -->
            <li class="dropdown user user-menu">
               <!-- Menu Toggle Button -->
               <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- The user image in the navbar-->
                  <img src="{{ asset("node_modules/admin-lte/dist/img/user2-160x160.jpg")}}" class="user-image" alt="User Image">
                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                  <span class="hidden-xs"> {{ Auth::user()->name }}</span>
               </a>
               <ul class="dropdown-menu">
                  <!-- The user image in the menu -->
                  <li class="user-header">
                     <img src="{{asset("node_modules/admin-lte/dist/img/user2-160x160.jpg")}}" class="img-circle" alt="User Image">
                     <p>
                        {{ Auth::user()->email }} <br> Web Developer
                        <small>Member since  {{ Auth::user()->created_at->format('M Y') }}</small>
                     </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                     <div class="pull-left">
                        <a href="{{action('UsersController@profile', ['id' => Auth::user()->id])}}" class="btn btn-default btn-flat">Profile</a>
                     </div>
                     <div class="pull-right">
                        <a href="{{action('HomeController@logout')}}" class="btn btn-default btn-flat">Sign out</a>
                     </div>
                  </li>
               </ul>
            </li>
            <!-- Control Sidebar Toggle Button -->
         </ul>
      </div>
   </nav>
</header>