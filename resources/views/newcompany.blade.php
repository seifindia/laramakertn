   <form role="form" method="post" action="config/company" class="" enctype="multipart/form-data">
		              <div class="box-body">
	          				
	          				<div class="row">
	          					<div class="col-md-6">
	          						<div class="form-group">
			          			<input class="form-control" type="text" placeholder="company name" name="full_name" required="required">
	        				</div>
							<div class="form-group">
	          					<input class="form-control" type="text" placeholder="short_name" name="short_name" required="required">
	        				</div>

							<div class="form-group">
	          					<input class="form-control" type="email" placeholder="email" name="email" required="required">
	        				</div>

							<div class="form-group">
	          					<input class="form-control" type="url" placeholder="url" name="url" required="required">
	        				</div>

							<div class="form-group">
							<label>Favicon</label>
	          					<input class="form-control" type="file" placeholder="favicon" name="favicon" >
	        				</div>

							<div class="form-group">
	          					<input class="form-control" type="text" placeholder="adress" name="adress" >
	        				</div>

							<div class="form-group">
	          					<input class="form-control" type="text" placeholder="phone" name="phone" >
	        				</div>

							

	          					</div>
	          					<div class="col-md-6">

	          					<div class="form-group">
	          					<textarea class="form-control" placeholder="description" name="description" rows="2"></textarea>
	        				</div>


	          						<div class="form-group">
								<label>Social networks:</label>
	        				</div>

							<div class="form-group">
	          					<input class="form-control" type="text" placeholder="youtube" name="youtube" >
	        				</div>
							<div class="form-group">
	          					<input class="form-control" type="text" placeholder="facebook" name="facebook" >
	        				</div>
							<div class="form-group">
	          					<input class="form-control" type="text" placeholder="twitter" name="twitter" >
	        				</div>
							<div class="form-group">
	          					<input class="form-control" type="text" placeholder="linkedin" name="linkedin" >
	        				</div>
	          					</div>
	          				</div>
							


		              </div>
		              <!-- /.box-body -->
		              <div class="box-footer">
		                	<button type="submit" class="btn btn-info">Save</button>
				       			
				       		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		              </div>
		            </form>