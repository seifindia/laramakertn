<h2>
	Category title: {{$category->title}}
</h2>
<ul>
	@foreach ($category->blocks as $blk)
		<li>{{$blk->sub_title}}</li>
	@endforeach
</ul>