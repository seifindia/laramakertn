
	@extends ('admin_tmpl')
	@section('mystyles')
	  <link rel="stylesheet" href="{{asset("/node_modules/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css")}}">
	  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset("/node_modules/admin-lte/plugins/datatables/dataTables.bootstrap.css")}}">
    <!-- Select2 -->
  <link rel="stylesheet" href="{{asset("/node_modules/admin-lte/plugins/select2/select2.min.css")}}">
	@endsection

	@section('page_title', 'Edit Page')
	@section ('content')
	<div class="row" id="edit-form">
	<div class="col-md-12">
	<form class="form-horizontal" method="POST" enctype="multipart/form-data" action="{{action('PagesController@update', ['id' => $page->id])}}">
	<div class="col-md-12">
	<div class="box box-info">
	<div class="box-header">
	<h3 class="box-title">Update '{{$page->title}}'</h3>
	<div class="box-tools pull-right">
	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
	</button>
	</div>
	<!-- /.box-tools -->
	</div>
	<!-- /.box-header -->
	<div class="box-body pad">
	<div class="col-md-12">
	<div class="form-group">
	<div class="row">
			<div class="col-md-9">
		<label>Title</label>
		<input class="form-control" type="text" placeholder="Title" name="title" required="required" value="{{$page->title}}">
	</div>
	<div class="col-md-3">
	<label>Status</label><br>
	<select class="form-control select2" name="state">
		<option value="1" @if($page->state == 1) selected @endif>Pusblished</option>
		<option value="2" @if($page->state == 2) selected @endif>Review</option>
		<option value="0" @if($page->state == 0) selected @endif>Draft</option>
	</select>
	</div>
	</div>
	</div>
	<div class="form-group">
		<div class="col-md-6">
			<div class="form-group">
				<label>Keywords</label>
				<input class="form-control" type="text" placeholder="Keywords" name="keywords" value="{{$page->keywords}}" style="width: 90%">
			</div>		
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label>Description</label>
				<input class="form-control" type="text" placeholder="Description" name="description" value="{{$page->description}}" style="width: 100%">
			</div>
		</div>
	</div>

	<div class="form-group">
	<label>Content</label>
	<textarea id="editor1" class="textarea col-md-12" name="content" rows="5"  placeholder="This is my textarea to be replaced with page content.">
		{{$page->content}}
	</textarea>                  	
	</div>
	<div class="form-group">
	
	<div class="col-md-6" id="uploadfiles">
	<div class="form-group">
	<label>Language : </label>
  @foreach ($languages as $lang)
  	<span class="@if($lang->code_lang != $page->code_lang) hidden @endif">{{$lang->name}}</span>
  @endforeach

	</div> 

            <div class="form-group">
              <label for="exampleInputFile">Image</label>
                 @if ($page->img_name != NULL)
                 <p class="img_name">
	                   
                     	 <a href="../../page/imfzxage/{{ $page->id }}" class="del_a">
                        <i class="fa fa-trash"></i>
                      </a>
                  
                  <a href="{{asset('front/'.$page->img_name)}}" data-lightbox="image-1">{{$page->img_name}}</a>
                 </p>
                 @else 
              <input type="file" id="page-image" name="img_name">
                  @endif
            </div>

	            		<div class="form-group">
                  <div class="input-group">
                     <label for="exampleInputFile">Css File(s)</label>
                 @if ($page->css_url != NULL)
                 <p class="css_url">
                      <a href="../../page/css/{{ $page->id }}" class="del_a">
                        <i class="fa fa-trash"></i>
                      </a>
                  <a href="{{asset("/front/$page->css_url")}}" target="_blank">{{$page->css_url}}</a>
                 </p>
                  @else
					<input type="file" id="page-css" name="css_url">
                  @endif
                  </div>
	            		</div>
	            		<div class="form-group">
                  <div class="input-group">
                      <label for="exampleInputFile">Js File(s)</label>
                 @if ($page->js_url != NULL)
                 <p class="js_url">
                       <a href="../../page/js/{{ $page->id }}" class="del_a">
                        <i class="fa fa-trash"></i>
                      </a>
					<a href="{{asset("/front/$page->js_url")}}" target="_blank">{{$page->js_url}}</a>
                 </p>
                  @else
                      <input type="file" id="page-js" name="js_url"> 
                  @endif
                  </div>
	            		</div>

	</div>


<div class="col-md-6">
            <div class="form-group">
                <label>Layout</label><br>
                <select class="form-control select2" requied="requied" style="width: 50%;" name="page_layout">
                    <option value="1" @if($page->page_layout == 1) selected @endif>Full width</option>
                    <option value="2" @if($page->page_layout == 2) selected @endif>Centred</option>
                    <option value="3" @if($page->page_layout == 3) selected @endif>Right Sidebar</option>
                    <option value="4" @if($page->page_layout == 4) selected @endif>Left Sidebar</option>
                </select>
            </div>

	<div class="form-group">
	<label>URL</label>
<input class="form-control" type="text" placeholder="url"
name="page_url" 
value="{{$page->code_lang}}/{{$page->title}}">
</div>

	<div class="form-group">
			 <textarea class="form-control" rows="4" placeholder="Enter your javascript code ..." name="js_code">{{$page->js_code}}</textarea>
	</div>

</div>

	</div>			

	<input type="hidden" name="code_lang" value="{{$page->code_lang}}">
	<input name="_method" type="hidden" value="PATCH">
    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
	<input type="hidden" name="_token" value="{{ csrf_token() }}"/>
	<div class="form-group">
	<button type="submit" class="btn btn-info">Save</button>
	</div>
	</div>
	</div>
	<!-- /.boxbody -->
	</div><!-- box-info -->
	</div>
	</form>
	</div>
	</div>
	@section ('myscripts')
			<!-- CK Editor -->
			<script src="{{asset("/node_modules/admin-lte/plugins/ck/ckeditor.js")}}"></script>
			<!-- Bootstrap WYSIHTML5 -->
			<script src="{{asset("/node_modules/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js")}}"></script>

<!-- SlimScroll -->
<script src="{{asset("/node_modules/admin-lte/plugins/slimScroll/jquery.slimscroll.min.js")}}"></script>
<!-- FastClick -->
<script src="{{asset("/node_modules/admin-lte/plugins/fastclick/fastclick.js")}}"></script>
<!-- Select2 -->
<script src="{{asset("/node_modules/admin-lte/plugins/select2/select2.full.min.js")}}"></script>
<script>
			  $(function () {
			    // Replace the <textarea id="editor1"> with a CKEditor
			    // instance, using default configuration.
			 //   CKEDITOR.replace('editor1');
			    //bootstrap WYSIHTML5 - text editor
			 

			     CKEDITOR.replace('editor1');
			     CKEDITOR.config.startupMode = 'source';
			     
   CKEDITOR.config.filebrowserBrowseUrl = '../../mediaselect';

			     //CKEDITOR.config.extraPlugins = 'filebrowser';
//			     CKEDITOR.config.startupMode = 'source';

			     //$(".editor1").wysihtml5();
				$(".select2").select2();

				var nbr = 0;
				setTimeout(function(){ 
						
				$("#cke_33").click(function(){
					nbr++;
					if (nbr % 2 === 0) { 
					$("#notif").addClass("hidden"); }
   					else { $("#notif").removeClass("hidden");}
				});


					 }, 1000);

			});
			</script>
		@endsection

	@endsection